package com.behametrics.demoapp.MainActivityFragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.behametrics.demoapp.R;


public class MainFragment extends DialogFragment {

    public MainFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        getView().findViewById(R.id.btnShowMoreFragments).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NestedFragment nestedFragment = new NestedFragment();
                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                transaction.add(R.id.mainFragmentLayout, nestedFragment).commit();
            }
        });
    }
}
