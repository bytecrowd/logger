package com.behametrics.demoapp;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.AlertDialog;
import android.content.Intent;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.behametrics.annotations.Logger;
import com.behametrics.demoapp.MainActivityFragments.MainFragment;
import com.behametrics.logger.Behametrics;
import com.behametrics.generated.MainActivityLogger;
import com.github.nkzawa.emitter.Emitter;

import xdroid.toaster.Toaster;


@Logger()
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MainActivityLogger.init(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btnSwitchActivity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent secondIntent = new Intent(MainActivity.this, SecondActivity.class);
                MainActivity.this.startActivity(secondIntent);
            }
        });

        findViewById(R.id.btnStartNewSession).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Behametrics.startNewSession();
            }
        });

        findViewById(R.id.btnShowDialogFragment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogFragment();
            }
        });

        findViewById(R.id.btnShowEditDialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showEditDialog();
            }
        });

        findViewById(R.id.btnShowPopup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup();
            }
        });

        findViewById(R.id.btnToggleKeyboard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleDisplayKeyboard();
            }
        });

        findViewById(R.id.btnAuth).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Behametrics.getServerSocket() != null) {
                    Behametrics.getServerSocket().sendAuthenticate();
                }
            }
        });

        findViewById(R.id.btnLearn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Behametrics.getServerSocket() != null) {
                    Behametrics.getServerSocket().sendLearn();
                }
            }
        });

        Behametrics.onAuth(new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(MainActivity.this.getLocalClassName(), "onAuth response: " + args[0]);
                Toaster.toast(String.valueOf(args[0]));
            }
        });
    }

    private void showDialogFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment previousFragment = getSupportFragmentManager().findFragmentByTag("dialog");
        if (previousFragment != null) {
            fragmentTransaction.remove(previousFragment);
        }
        fragmentTransaction.addToBackStack(null);

        DialogFragment dialogFragment = new MainFragment();
        dialogFragment.show(fragmentTransaction, "dialog");
    }

    private void toggleDisplayKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
    }

    private void showEditDialog() {
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Test Input");
        alertDialogBuilder.setMessage("Enter arbitrary text: ");
        alertDialogBuilder.setView(input);
        AlertDialog dialog = alertDialogBuilder.show();

        Behametrics.getTouchLogger().start(dialog);
    }

    private void showPopup() {
        final PopupWindow popupWindow = new PopupWindow(MainActivity.this);

        LinearLayout layout = new LinearLayout(MainActivity.this);
        layout.setOrientation(LinearLayout.VERTICAL);

        TextView textView = new TextView(MainActivity.this);
        textView.setText(R.string.example_text);
        textView.setWidth(500);
        textView.setHeight(500);
        layout.addView(textView);

        Button btnDismiss = new Button(MainActivity.this);
        btnDismiss.setText(R.string.dismiss);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

        layout.addView(btnDismiss);

        popupWindow.setContentView(layout);
        popupWindow.showAtLocation(MainActivity.this.findViewById(R.id.mainActivityLayout), Gravity.CENTER, 0, 0);
        Behametrics.getTouchLogger().start(popupWindow);
    }
}
