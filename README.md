# Behametrics Logger

### NOTE: [A newer version of the logger](https://gitlab.com/behametrics/logger-android) is available that is actively maintained.

This is an Android library allowing you to easily log data in Android applications
from the touch screen, sensors (accelerometer, gyroscope, etc.) and additional inputs.

Logs are stored as CSV files for each input separately
and can be optionally sent to a server as JSON objects (see [Configuring Logger](#Configuring logger) below).


## Requirements

* Android SDK version 19 or later
* Java 8 or later


## Installation

Add maven URL into your root `build.gradle` file:
```
allprojects {
    repositories {
        ...
        maven { url "https://jitpack.io" }
    }
}
```

Add the following dependencies into your `app/build.gradle` file:
```
dependencies {
    implementation 'com.gitlab.bytecrowd:logger:X.X.X'
    implementation 'com.gitlab.bytecrowd:logger:processor:X.X.X'
    implementation 'com.gitlab.bytecrowd:logger:authentication:X.X.X'
    implementation 'com.gitlab.bytecrowd.logger:annotations:X.X.X'
    annotationProcessor 'com.gitlab.bytecrowd:logger:processor:X.X.X'
}
```


## Usage

### Logging in the entire application
To enable application-wide logging:
1. Mark your main Android activity class with `@Logger()` annotation.
2. Add `[class name]Logger.init(this)` at the top of the `onCreate()` method.
3. Build the application.

Example:
```java
@Logger()
public class MainActivity extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        MainActivityLogger.init(this);   // Add this before `super.onCreate()`
        super.onCreate(savedInstanceState);
    }
}

```

### Logging in specific places
For more fine-grained control over the logging process, directly invoke logging methods from the `Behametrics` class.

The following example enables logging in a specific activity only and stops logging on pausing or switching to another activity:
```java
public class ClassName extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Behametrics.init(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Behametrics.startLogging(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Behametrics.stopLogging(this);
    }
}
```

### Logging custom events
To log custom events, simply invoke `Behametrics.log()` and pass strings representing the data.

Example:
```
Button btn = findViewById(R.id.btn);
btnBack.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Behametrics.log("Button clicked", "MainActivity");
    }
});
```


## Configuring logger

To customize logging behavior, create a file named `config.properties` in `app/src/main/assets`.
For possible entries and their default values, see the [default `config.properties`](logger/src/main/assets/config.properties) file.


## Limitations

### Logging touch data from dialogs and popup windows

Due to Android API limitations, touch data from `Dialog` and `PopupWindow` instances
cannot be logged automatically and must be explicitly controlled for each instance.
`DialogFragment` instances do not need this special handling.

To enable touch data logging from `Dialog`:
```
Behametrics.getTouchLogger().start(dialog);
```

For `PopupWindow` instances, you have two options:
* Enable logging manually the same way as dialogs. Internally, a new `View.OnTouchListener` is connected to the popup.
If you are in the need of using a custom listener, use the second option.
```
Behametrics.getTouchLogger().start(popupWindow);
```
* Call `PopupWindow.setTouchInterceptor()` and pass your own `View.OnTouchListener`.
Call `Behametrics.getTouchLogger().processTouchEvent()` at the beginning of the `onTouch()` method.
```
popupWindow.setTouchInterceptor(new View.OnTouchListener() {
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Behametrics.getTouchLogger().processTouchEvent(event);
        // your code
        // ...
    }
});
```

Conversely, to stop logging, call `Behametrics.getTouchLogger().stop()`.
For the second option for `PopupWindow` instances,
pass a listener that does not call `Behametrics.getTouchLogger().processTouchEvent()`.

### Logging touch data from soft keyboard

Touch data cannot be logged from the built-in soft keyboard. If needed, use a library providing a
custom soft keyboard (or build your own).

### Overriding `Window.Callback`

The logger implements the
[`Window.Callback`](https://developer.android.com/reference/android/view/Window.Callback) interface to log touch data.
The class implementing this interface is hooked into each activity at the start of logging.
If you use a custom `Window.Callback` implementation in your application,
make sure that your implementation is added before the start of logging or removed before the end of logging.
Otherwise, you may encounter duplicate logging of touch data if the activity is resumed or created anew.
