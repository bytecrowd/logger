package com.behametrics.logger;

import android.app.Activity;

import com.behametrics.logger.InputLoggers.InputEvents.OrientationEvent;
import com.behametrics.logger.InputLoggers.OrientationLogger;
import com.behametrics.logger.InputLoggers.VariableArgEventLogger;
import com.behametrics.logger.InputLoggers.InputEvents.SensorEvent;
import com.behametrics.logger.InputLoggers.InputEvents.TouchEvent;
import com.behametrics.logger.InputLoggers.SensorLogger;
import com.behametrics.logger.InputLoggers.TouchLogger;

import java.util.ArrayList;
import java.util.List;

/**
 * Allows to start and stop logging of the specified inputs.
 */
public class InputLoggerManager {
    private static final String LIFECYCLE_EVENT_NAME = "lifecycle";
    private static final String CUSTOM_EVENT_NAME = "custom";
    private static final String ALL_INPUTS = "all";

    private TouchLogger touchLogger;
    private SensorLogger sensorLogger;
    private OrientationLogger orientationLogger;
    private VariableArgEventLogger lifecycleEventLogger;
    private VariableArgEventLogger customEventLogger;

    /**
     * Creates an input logger manager.
     *
     * @param inputEventDispatcher input event dispatcher
     */
    InputLoggerManager(InputEventDispatcher inputEventDispatcher) {
        touchLogger = new TouchLogger(inputEventDispatcher);
        sensorLogger = new SensorLogger(inputEventDispatcher);
        orientationLogger = new OrientationLogger(inputEventDispatcher);
        lifecycleEventLogger = new VariableArgEventLogger(LIFECYCLE_EVENT_NAME, inputEventDispatcher);
        customEventLogger = new VariableArgEventLogger(CUSTOM_EVENT_NAME, inputEventDispatcher);
    }

    TouchLogger getTouchLogger() {
        return touchLogger;
    }

    SensorLogger getSensorLogger() {
        return sensorLogger;
    }

    OrientationLogger getOrientationLogger() {
        return orientationLogger;
    }

    VariableArgEventLogger getLifecycleEventLogger() {
        return lifecycleEventLogger;
    }

    VariableArgEventLogger getCustomEventLogger() {
        return customEventLogger;
    }

    /**
     * Starts logging events from the specified inputs in the specified activity.
     *
     * @param activity      activity to start logging in
     * @param inputNames    list of input names
     * @param sensorLogRate log rate for sensors
     */
    void startLogging(Activity activity, String[] inputNames, String sensorLogRate) {
        boolean logTouchEvents = false;
        List<String> sensorNames = new ArrayList<>();
        boolean logOrientationEvents = false;
        boolean logLifecycleEvents = false;
        boolean logCustomEvents = false;

        boolean logAllInputs = false;

        for (String inputName : inputNames) {
            if (TouchEvent.INPUT_NAME.equals(inputName)) {
                logTouchEvents = true;
            } else if (OrientationEvent.INPUT_NAME.equals(inputName)) {
                logOrientationEvents = true;
            } else if (LIFECYCLE_EVENT_NAME.equals(inputName)) {
                logLifecycleEvents = true;
            } else if (CUSTOM_EVENT_NAME.equals(inputName)) {
                logCustomEvents = true;
            } else if (ALL_INPUTS.equals(inputName)) {
                logAllInputs = true;
                break;
            } else if (inputName.startsWith(SensorEvent.SENSOR_NAME_PREFIX)) {
                sensorNames.add(inputName.substring(SensorEvent.SENSOR_NAME_PREFIX.length()));
            }
        }

        if (logAllInputs) {
            touchLogger.start(activity);
            orientationLogger.start(activity);
            lifecycleEventLogger.start();
            customEventLogger.start();
            sensorLogger.start(activity, new String[]{SensorLogger.ALL_SENSORS}, sensorLogRate);
        } else {
            if (logTouchEvents) {
                touchLogger.start(activity);
            }
            if (logOrientationEvents) {
                orientationLogger.start(activity);
            }
            if (logLifecycleEvents) {
                lifecycleEventLogger.start();
            }
            if (logCustomEvents) {
                customEventLogger.start();
            }
            sensorLogger.start(activity, sensorNames.toArray(new String[0]), sensorLogRate);
        }
    }

    /**
     * Stops logging events in the specified activity.
     *
     * @param activity activity to stop logging in
     */
    void stopLogging(Activity activity) {
        touchLogger.stop(activity);
        orientationLogger.stop(activity);
        lifecycleEventLogger.stop();
        customEventLogger.stop();
        sensorLogger.stop(activity);
    }
}
