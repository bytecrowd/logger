package com.behametrics.logger.Callbacks;

import android.app.Activity;
import android.os.Build;
import android.support.annotation.Nullable;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SearchEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;

import com.behametrics.logger.InputLoggers.TouchLogger;

/**
 * Object responsible for intercepting and logging touch events from all views in the specified
 * activity, including nested views and views added dynamically.
 */
public class TouchEventHandler implements Window.Callback {

    private TouchLogger touchLogger;
    private Window.Callback callback;

    public TouchEventHandler(TouchLogger touchLogger, Window.Callback callback) {
        this.touchLogger = touchLogger;
        this.callback = callback;
    }

    public TouchEventHandler(TouchLogger touchLogger, Activity activity) {
        this.touchLogger = touchLogger;
        this.callback = activity.getWindow().getCallback();
    }

    public Window.Callback getCallback() {
        return callback;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return callback.dispatchKeyEvent(keyEvent);
    }

    @Override
    public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
        return callback.dispatchKeyShortcutEvent(keyEvent);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        touchLogger.processTouchEvent(motionEvent);
        return callback.dispatchTouchEvent(motionEvent);
    }

    @Override
    public boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        return callback.dispatchTrackballEvent(motionEvent);
    }

    @Override
    public boolean dispatchGenericMotionEvent(MotionEvent motionEvent) {
        return callback.dispatchGenericMotionEvent(motionEvent);
    }

    @Override
    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        return callback.dispatchPopulateAccessibilityEvent(accessibilityEvent);
    }

    @Nullable
    @Override
    public View onCreatePanelView(int i) {
        return callback.onCreatePanelView(i);
    }

    @Override
    public boolean onCreatePanelMenu(int i, Menu menu) {
        return callback.onCreatePanelMenu(i, menu);
    }

    @Override
    public boolean onPreparePanel(int i, View view, Menu menu) {
        return callback.onPreparePanel(i, view, menu);
    }

    @Override
    public boolean onMenuOpened(int i, Menu menu) {
        return callback.onMenuOpened(i, menu);
    }

    @Override
    public boolean onMenuItemSelected(int i, MenuItem menuItem) {
        return callback.onMenuItemSelected(i, menuItem);
    }

    @Override
    public void onWindowAttributesChanged(WindowManager.LayoutParams layoutParams) {
        callback.onWindowAttributesChanged(layoutParams);
    }

    @Override
    public void onContentChanged() {
        callback.onContentChanged();
    }

    @Override
    public void onWindowFocusChanged(boolean b) {
        callback.onWindowFocusChanged(b);
    }

    @Override
    public void onAttachedToWindow() {
        callback.onAttachedToWindow();
    }

    @Override
    public void onDetachedFromWindow() {
        callback.onDetachedFromWindow();
    }

    @Override
    public void onPanelClosed(int i, Menu menu) {
        callback.onPanelClosed(i, menu);
    }

    @Override
    public boolean onSearchRequested() {
        return callback.onSearchRequested();
    }

    @Override
    public boolean onSearchRequested(SearchEvent searchEvent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return callback.onSearchRequested(searchEvent);
        }
        else {
            return callback.onSearchRequested();
        }
    }

    @Nullable
    @Override
    public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
        return this.callback.onWindowStartingActionMode(callback);
    }

    @Nullable
    @Override
    public ActionMode onWindowStartingActionMode(ActionMode.Callback callback, int i) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return this.callback.onWindowStartingActionMode(callback, i);
        }
        else {
            return this.callback.onWindowStartingActionMode(callback);
        }
    }

    @Override
    public void onActionModeStarted(ActionMode actionMode) {
        callback.onActionModeStarted(actionMode);
    }

    @Override
    public void onActionModeFinished(ActionMode actionMode) {
        callback.onActionModeFinished(actionMode);
    }
}
