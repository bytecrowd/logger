package com.behametrics.logger.Callbacks;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;

import com.behametrics.logger.Behametrics;

import java.util.HashMap;

/**
 * Enables logging of touch data from {@link android.app.DialogFragment} and {@link DialogFragment}
 * instances.
 */
public class FragmentLifecycleCallbackManager {

    private static HashMap<Activity, FragmentLifeTracker> activitiesAndFragmentLifeTrackers;

    public FragmentLifecycleCallbackManager() {
        activitiesAndFragmentLifeTrackers = new HashMap<>();
    }

    public boolean registerCallbacks(Activity activity) {
        boolean registrationSuccessful = true;

        if (activity instanceof FragmentActivity) {
            SupportFragmentLifeTracker supportFragmentLifeTracker = new SupportFragmentLifeTracker();
            ((FragmentActivity)activity).getSupportFragmentManager().registerFragmentLifecycleCallbacks(supportFragmentLifeTracker, true);
            activitiesAndFragmentLifeTrackers.put(activity, supportFragmentLifeTracker);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                StandardFragmentLifeTracker standardFragmentLifeTracker = new StandardFragmentLifeTracker();
                activity.getFragmentManager().registerFragmentLifecycleCallbacks(standardFragmentLifeTracker, true);
                activitiesAndFragmentLifeTrackers.put(activity, standardFragmentLifeTracker);
            } else {
                registrationSuccessful = false;
            }
        }

        return registrationSuccessful;
    }

    public void unregisterCallbacks(Activity activity) {
        FragmentLifeTracker fragmentLifetracker = activitiesAndFragmentLifeTrackers.get(activity);
        if (fragmentLifetracker == null) {
            return;
        }

        if (activity instanceof FragmentActivity) {
            ((FragmentActivity)activity).getSupportFragmentManager().unregisterFragmentLifecycleCallbacks((SupportFragmentLifeTracker)fragmentLifetracker);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                activity.getFragmentManager().unregisterFragmentLifecycleCallbacks((StandardFragmentLifeTracker)fragmentLifetracker);
            }
        }

        activitiesAndFragmentLifeTrackers.remove(activity);
    }
}


interface FragmentLifeTracker {
}


@RequiresApi(api = Build.VERSION_CODES.O)
class StandardFragmentLifeTracker extends android.app.FragmentManager.FragmentLifecycleCallbacks implements FragmentLifeTracker {

    @Override
    public void onFragmentResumed(android.app.FragmentManager fm, android.app.Fragment f) {
        super.onFragmentResumed(fm, f);

        Behametrics.logLifecycleEvent("fragment", "resumed", f.getClass().getName());

        if (f instanceof android.app.DialogFragment) {
            Behametrics.getTouchLogger().start((android.app.DialogFragment)f);
        }
    }

    @Override
    public void onFragmentPaused(android.app.FragmentManager fm, android.app.Fragment f) {
        super.onFragmentPaused(fm, f);

        Behametrics.logLifecycleEvent("fragment", "paused", f.getClass().getName());

        if (f instanceof android.app.DialogFragment) {
            Behametrics.getTouchLogger().stop((android.app.DialogFragment)f);
        }
    }

    @Override
    public void onFragmentAttached(android.app.FragmentManager fm, android.app.Fragment f, Context context) {
        super.onFragmentAttached(fm, f, context);

        Behametrics.logLifecycleEvent("fragment", "attached", f.getClass().getName());
    }

    @Override
    public void onFragmentCreated(android.app.FragmentManager fm, android.app.Fragment f, Bundle savedInstanceState) {
        super.onFragmentCreated(fm, f, savedInstanceState);

        Behametrics.logLifecycleEvent("fragment", "created", f.getClass().getName());
    }

    @Override
    public void onFragmentStarted(android.app.FragmentManager fm, android.app.Fragment f) {
        super.onFragmentStarted(fm, f);

        Behametrics.logLifecycleEvent("fragment", "started", f.getClass().getName());
    }

    @Override
    public void onFragmentStopped(android.app.FragmentManager fm, android.app.Fragment f) {
        super.onFragmentStopped(fm, f);

        Behametrics.logLifecycleEvent("fragment", "stopped", f.getClass().getName());
    }

    @Override
    public void onFragmentDestroyed(android.app.FragmentManager fm, android.app.Fragment f) {
        super.onFragmentDestroyed(fm, f);

        Behametrics.logLifecycleEvent("fragment", "destroyed", f.getClass().getName());
    }

    @Override
    public void onFragmentDetached(android.app.FragmentManager fm, android.app.Fragment f) {
        super.onFragmentDetached(fm, f);

        Behametrics.logLifecycleEvent("fragment", "detached", f.getClass().getName());
    }
}

class SupportFragmentLifeTracker extends FragmentManager.FragmentLifecycleCallbacks implements FragmentLifeTracker {

    @Override
    public void onFragmentResumed(@NonNull FragmentManager fm, @NonNull Fragment f) {
        super.onFragmentResumed(fm, f);

        Behametrics.logLifecycleEvent("fragment", "resumed", f.getClass().getName());

        if (f instanceof DialogFragment) {
            Behametrics.getTouchLogger().start((DialogFragment)f);
        }
    }

    @Override
    public void onFragmentPaused(@NonNull FragmentManager fm, @NonNull Fragment f) {
        super.onFragmentPaused(fm, f);

        Behametrics.logLifecycleEvent("fragment", "paused", f.getClass().getName());

        if (f instanceof DialogFragment) {
            Behametrics.getTouchLogger().stop((DialogFragment)f);
        }
    }

    @Override
    public void onFragmentAttached(@NonNull FragmentManager fm, @NonNull Fragment f, @NonNull Context context) {
        super.onFragmentAttached(fm, f, context);

        Behametrics.logLifecycleEvent("fragment", "attached", f.getClass().getName());
    }

    @Override
    public void onFragmentCreated(@NonNull FragmentManager fm, @NonNull Fragment f, @Nullable Bundle savedInstanceState) {
        super.onFragmentCreated(fm, f, savedInstanceState);

        Behametrics.logLifecycleEvent("fragment", "created", f.getClass().getName());
    }

    @Override
    public void onFragmentStarted(@NonNull FragmentManager fm, @NonNull Fragment f) {
        super.onFragmentStarted(fm, f);

        Behametrics.logLifecycleEvent("fragment", "started", f.getClass().getName());
    }

    @Override
    public void onFragmentStopped(@NonNull FragmentManager fm, @NonNull Fragment f) {
        super.onFragmentStopped(fm, f);

        Behametrics.logLifecycleEvent("fragment", "stopped", f.getClass().getName());
    }

    @Override
    public void onFragmentDestroyed(@NonNull FragmentManager fm, @NonNull Fragment f) {
        super.onFragmentDestroyed(fm, f);

        Behametrics.logLifecycleEvent("fragment", "destroyed", f.getClass().getName());
    }

    @Override
    public void onFragmentDetached(@NonNull FragmentManager fm, @NonNull Fragment f) {
        super.onFragmentDetached(fm, f);

        Behametrics.logLifecycleEvent("fragment", "detached", f.getClass().getName());
    }
}
