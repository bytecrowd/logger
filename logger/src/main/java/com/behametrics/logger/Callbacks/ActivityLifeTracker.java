package com.behametrics.logger.Callbacks;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.behametrics.logger.Behametrics;

public class ActivityLifeTracker implements Application.ActivityLifecycleCallbacks {
    private FragmentLifecycleCallbackManager fragmentLifecycleCallbackManager;

    public ActivityLifeTracker() {
        fragmentLifecycleCallbackManager = new FragmentLifecycleCallbackManager();
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        Behametrics.init(activity);
        Behametrics.logLifecycleEvent("activity", "created", activity.getClass().getName());
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Behametrics.logLifecycleEvent("activity", "started", activity.getClass().getName());
    }

    @Override
    public void onActivityResumed(Activity activity) {
        Behametrics.startLogging(activity);
        Behametrics.logLifecycleEvent("activity", "resumed", activity.getClass().getName());

        fragmentLifecycleCallbackManager.registerCallbacks(activity);
    }

    @Override
    public void onActivityPaused(Activity activity) {
        Behametrics.logLifecycleEvent("activity", "paused", activity.getClass().getName());

        fragmentLifecycleCallbackManager.unregisterCallbacks(activity);
        Behametrics.stopLogging(activity);
    }

    @Override
    public void onActivityStopped(Activity activity) {
        Behametrics.logLifecycleEvent("activity", "stopped", activity.getClass().getName());
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Behametrics.logLifecycleEvent("activity", "destroyed", activity.getClass().getName());
    }
}
