package com.behametrics.logger;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.behametrics.authentication.ServerSocket;
import com.behametrics.logger.InputLoggers.TouchLogger;
import com.behametrics.logger.Utils.StoragePathManager;
import com.github.nkzawa.emitter.Emitter;

import java.util.UUID;

import com.behametrics.logger.Utils.Properties.PropertyReader;

/**
 * Object providing high-level API for adding sensor logging capabilities to applications.
 */
public class Behametrics {
    private static final String CONFIG_FILENAME = "config.properties";
    private static final String CONFIG_SENSORS_SEPARATOR = ",";

    private static Behametrics behametrics = null;

    private static SharedPreferences wmbPreference;
    private static PropertyReader propertyReader;
    private static ServerSocket serverSocket;
    private static InputEventDispatcher inputEventDispatcher;
    private static InputLoggerManager inputLoggerManager;

    private Behametrics(Context context) {
        loadProperties(context);

        wmbPreference = PreferenceManager.getDefaultSharedPreferences(context);
        setSharedPrefsOnFirstRun();

        initLogging(context);

        setServerSocket();
    }

    /**
     * Initializes logging. Subsequent calls to this method have no effect.
     *
     * @param context context instance. The method uses the application context (via
     *                {@link Context#getApplicationContext()}) to avoid potential memory leaks
     *                associated with held references to {@link Activity} instances.
     */
    public static void init(Context context) {
        if (behametrics == null) {
            behametrics = new Behametrics(context.getApplicationContext());
        }
    }

    /**
     * Returns a property reader to read configuration entries.
     *
     * @return
     */
    public static PropertyReader getPropertyReader() {
        return propertyReader;
    }

    /**
     * Starts logging. Consecutive calls to this method have no effect unless the list of sensors to
     * log data from changed at runtime, in which case sensors not logged before are now logged.
     *
     * @param activity activity to log events in
     */
    public static void startLogging(Activity activity) {
        inputLoggerManager.startLogging(
                activity,
                propertyReader.getStringArrayProperty("Inputs", CONFIG_SENSORS_SEPARATOR, true),
                propertyReader.getStringProperty("SensorLogRate"));
    }

    /**
     * Stops logging. Consecutive calls to this method have no effect.
     *
     * @param activity activity to stop logging events in
     */
    public static void stopLogging(Activity activity) {
        inputLoggerManager.stopLogging(activity);
    }

    /**
     * Logs a custom event containing the specified arguments.
     *
     * @param eventData strings representing custom event data
     */
    public static void log(String... eventData) {
        inputLoggerManager.getCustomEventLogger().log(eventData);
    }

    /**
     * Logs an event related to the lifecycle of an activity or fragment (started, paused, stopped,
     * ...).
     *
     * @param eventData strings representing a lifecycle event
     */
    public static void logLifecycleEvent(String... eventData) {
        inputLoggerManager.getLifecycleEventLogger().log(eventData);
    }

    /**
     * Returns the touch logger.
     *
     * @return touch logger
     */
    public static TouchLogger getTouchLogger() {
        return inputLoggerManager.getTouchLogger();
    }

    /**
     * Starts a new session. Under the hood, this method generates a new session ID that is attached
     * to logged events.
     *
     * @see InputEventDispatcher
     */
    public static void startNewSession() {
        inputEventDispatcher.setSessionId(generateSessionId());
    }

    public static ServerSocket getServerSocket() {
        return serverSocket;
    }

    public static void onAuth(Emitter.Listener listener) {
        if (serverSocket != null) {
            serverSocket.setAuth(listener);
        }
    }

    private void initLogging(Context context) {
        inputEventDispatcher = new InputEventDispatcher(
                getStorageDirpath(context),
                generateSessionId(),
                propertyReader);
        inputLoggerManager = new InputLoggerManager(inputEventDispatcher);
    }

    private static void setServerSocket() {
        String serverSocketUrl = propertyReader.getStringProperty("AuthSocketUrl");
        String serverUrl = propertyReader.getStringProperty("ServerUrl");

        if (!serverSocketUrl.isEmpty() && !serverUrl.isEmpty()) {
            serverSocket = new ServerSocket(
                    serverSocketUrl,
                    wmbPreference.getString("UUID", null),
                    serverUrl);
        }
    }

    private static void setSharedPrefsOnFirstRun() {
        boolean isFirstRun = wmbPreference.getBoolean("FIRSTRUN", true);
        if (isFirstRun) {
            String uniqueDevice = UUID.randomUUID().toString();

            SharedPreferences.Editor editor = wmbPreference.edit();
            editor.putString("UUID", uniqueDevice);
            editor.putBoolean("FIRSTRUN", false);
            editor.apply();
        }
    }

    private void loadProperties(Context context) {
        propertyReader = new PropertyReader();
        propertyReader.loadProperties(context, CONFIG_FILENAME);
    }

    private static String generateSessionId() {
        return System.currentTimeMillis() + "-" + wmbPreference.getString("UUID", "");
    }

    private static String getStorageDirpath(Context context) {
        return StoragePathManager.getStorageDirpath(context, propertyReader.getStringProperty("StorageType"));
    }
}
