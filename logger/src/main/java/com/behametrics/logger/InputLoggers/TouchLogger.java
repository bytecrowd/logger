package com.behametrics.logger.InputLoggers;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.PopupWindow;

import com.behametrics.logger.Callbacks.TouchEventHandler;
import com.behametrics.logger.InputEventDispatcher;
import com.behametrics.logger.InputLoggers.InputEvents.TouchEvent;
import com.behametrics.logger.Utils.Maps.MotionEventActionMap;

/**
 * Object logging touch screen events.
 * Currently supports finger touches only (i.e. no mouse, pen or trackball events are processed).
 *
 * @see MotionEvent
 */
public class TouchLogger extends InputLogger {
    private static final long NANOSECONDS_PER_MILLISECOND = 1000000;
    private static final String TOUCH_DOWN = "down";
    private static final String TOUCH_MOVE = "move";
    private static final String TOUCH_UP = "up";
    private static final String TOUCH_OTHER = "other";

    private static MotionEventActionMap motionEventActionMap;

    private long upTimeMilliseconds = System.currentTimeMillis() - SystemClock.uptimeMillis();

    /**
     * Creates a touch logger given the {@link InputEventDispatcher} instance.
     *
     * @param inputEventDispatcher input event dispatcher to pass logged events to for further
     *                             processing
     */
    public TouchLogger(InputEventDispatcher inputEventDispatcher) {
        setInputEventDispatcher(inputEventDispatcher);

        if (motionEventActionMap == null) {
            motionEventActionMap = new MotionEventActionMap();
        }
    }

    /**
     * Starts logging touch events in the specified activity. Consecutive calls to this method
     * without first calling {@link TouchLogger#stop} will have no effect.
     *
     * @param activity activity to log touch events in
     */
    public void start(Activity activity) {
        Window activityWindow = activity.getWindow();
        if (activityWindow == null) {
            return;
        }

        start(activityWindow);
    }

    /**
     * Starts logging touch events in the specified dialog. Consecutive calls to this method
     * without first calling {@link TouchLogger#stop} with the same dialog will have no effect.
     *
     * @param dialog dialog to log touch events in
     */
    public void start(Dialog dialog) {
        Window dialogWindow = dialog.getWindow();
        if (dialogWindow == null) {
            return;
        }

        start(dialogWindow);
    }

    /**
     * Starts logging touch events in the specified dialog fragment. Consecutive calls to this
     * method without first calling {@link TouchLogger#stop} with the same dialog fragment will have
     * no effect.
     *
     * @param dialogFragment dialog fragment to log touch events in
     */
    public void start(DialogFragment dialogFragment) {
        start(dialogFragment.getDialog());
    }

    /**
     * Starts logging touch events in the specified dialog fragment. Consecutive calls to this
     * method without first calling {@link TouchLogger#stop} with the same dialog fragment will have
     * no effect.
     *
     * @param dialogFragment dialog fragment to log touch events in
     */
    public void start(android.support.v4.app.DialogFragment dialogFragment) {
        start(dialogFragment.getDialog());
    }

    /**
     * Starts logging touch events in the specified popup window. Consecutive calls to this method
     * without first calling {@link TouchLogger#stop} with the same popup window will have no
     * effect.
     *
     * <p>Note: If the passed {@code popupWindow} already has a
     * {@link android.view.View.OnTouchListener} connected, it will be overridden. To prevent this
     * behavior, manually invoke {@link #processTouchEvent )} inside your listener (see
     * {@link #processTouchEvent ) for more information).</p>
     *
     * @param popupWindow popup window to log touch events in
     */
    public void start(PopupWindow popupWindow) {
        popupWindow.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                processTouchEvent(event);
                return false;
            }
        });
    }

    private void start(Window window) {
        if (!(window.getCallback() instanceof TouchEventHandler)) {
            window.setCallback(new TouchEventHandler(this, window.getCallback()));
        }
    }

    /**
     * Stops logging touch events in the specified activity. Calling this method before first
     * calling {@link TouchLogger#start} will have no effect.
     *
     * @param activity activity to stop logging touch events in
     */
    public void stop(Activity activity) {
        Window activityWindow = activity.getWindow();
        if (activityWindow == null) {
            return;
        }

        stop(activityWindow);
    }

    /**
     * Stops logging touch events in the specified dialog. Calling this method before first
     * calling {@link TouchLogger#start} with the same dialog will have no effect.
     *
     * @param dialog dialog to stop logging touch events in
     */
    public void stop(Dialog dialog) {
        Window dialogWindow = dialog.getWindow();
        if (dialogWindow == null) {
            return;
        }

        stop(dialogWindow);
    }

    /**
     * Stops logging touch events in the specified dialog. Calling this method before first
     * calling {@link TouchLogger#start} with the same dialog will have no effect.
     *
     * @param dialogFragment dialog to stop logging touch events in
     */
    public void stop(DialogFragment dialogFragment) {
        stop(dialogFragment.getDialog());
    }

    /**
     * Stops logging touch events in the specified dialog. Calling this method before first
     * calling {@link TouchLogger#start} with the same dialog will have no effect.
     *
     * @param dialogFragment dialog to stop logging touch events in
     */
    public void stop(android.support.v4.app.DialogFragment dialogFragment) {
        stop(dialogFragment.getDialog());
    }

    /**
     * Stops logging touch events in the specified popup window. Calling this method before first
     * calling {@link TouchLogger#stop} with the same popup window will have no effect.
     *
     * <p>Note: If the passed {@code popupWindow} already has a
     * {@link android.view.View.OnTouchListener} connected including custom code, it will be
     * overridden by an empty listener.</p>
     *
     * @param popupWindow popup window to stop logging touch events in
     */
    public void stop(PopupWindow popupWindow) {
        popupWindow.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });
    }

    private void stop(Window window) {
        Window.Callback callback = window.getCallback();

        if (callback instanceof TouchEventHandler) {
            Window.Callback origCallback = ((TouchEventHandler) callback).getCallback();
            window.setCallback(origCallback);
        }
    }

    /**
     * Processes the specified motion event and creates a touch event.
     *
     * <p>This method is called automatically when processing events after calling {@link #start}.
     * If, for some reason, you need to process touch events manually (such as for
     * {@link android.view.View.OnTouchListener} passed to
     * {@link android.widget.PopupWindow#setTouchInterceptor}), call this method explicitly.</p>
     *
     * @param event motion event to process
     * @see MotionEvent
     */
    public void processTouchEvent(MotionEvent event) {
        synchronized (this) {
            int action = event.getActionMasked();

            switch (action) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_POINTER_DOWN:
                    processTouchDown(event, action);
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_POINTER_UP:
                    processTouchUp(event, action);
                    break;
                case MotionEvent.ACTION_MOVE:
                    processTouchMove(event, action);
                    break;
                default:
                    processTouchOther(event, action);
                    break;
            }
        }
    }

    private void processTouchDown(MotionEvent event, int action) {
        addTouchEvent(event, TOUCH_DOWN, action, event.getActionIndex());
    }

    private void processTouchUp(MotionEvent event, int action) {
        addTouchEvent(event, TOUCH_UP, action, event.getActionIndex());
    }

    private void processTouchMove(MotionEvent event, int action) {
        for (int pointerIndex = 0; pointerIndex < event.getPointerCount(); pointerIndex++) {
            try {
                addTouchEvent(event, TOUCH_MOVE, action, pointerIndex);
            } catch (IndexOutOfBoundsException e) {
            }
        }
    }

    private void processTouchOther(MotionEvent event, int action) {
        addTouchEvent(event, TOUCH_OTHER, action, event.getActionIndex());
    }

    private void addTouchEvent(MotionEvent event, String eventTypeName, int actionType, int pointerIndex) {
        inputEventDispatcher.dispatch(new TouchEvent(
                (upTimeMilliseconds + event.getEventTime()) * NANOSECONDS_PER_MILLISECOND,
                inputEventDispatcher.getSessionId(),
                eventTypeName,
                motionEventActionMap.getName(actionType),
                event.getPointerId(pointerIndex),
                event.getX(pointerIndex),
                event.getY(pointerIndex),
                event.getPressure(pointerIndex),
                event.getSize(pointerIndex),
                event.getTouchMajor(pointerIndex),
                event.getTouchMinor(pointerIndex),
                event.getRawX(),
                event.getRawY()
        ));
    }
}
