package com.behametrics.logger.InputLoggers;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.SystemClock;

import java.util.List;

import com.behametrics.logger.InputEventDispatcher;
import com.behametrics.logger.InputLoggers.InputEvents.SensorEvent;
import com.behametrics.logger.Utils.Maps.SensorTypeMap;
import com.behametrics.logger.Utils.Maps.SensorDelayMap;

/**
 * Object logging events from all available sensors.
 *
 * @see Sensor
 */
public class SensorLogger extends InputLogger implements SensorEventListener {
    public static final String ALL_SENSORS = "all";

    private static final long NANOSECONDS_PER_MILLISECOND = 1000000;
    private static final int DEFAULT_SENSOR_DELAY = SensorManager.SENSOR_DELAY_GAME;

    private static SensorTypeMap sensorTypeMap;
    private static SensorDelayMap sensorDelayMap;

    private long upTimeNanoseconds =
            (System.currentTimeMillis() * NANOSECONDS_PER_MILLISECOND) - SystemClock.elapsedRealtimeNanos();

    /**
     * Creates an instance given the input event dispatcher.
     *
     * @param inputEventDispatcher  input event dispatcher to pass logged events to for further
     *                              processing
     */
    public SensorLogger(InputEventDispatcher inputEventDispatcher) {
        setInputEventDispatcher(inputEventDispatcher);

        if (sensorTypeMap == null) {
            sensorTypeMap = new SensorTypeMap();
        }

        if (sensorDelayMap == null) {
            sensorDelayMap = new SensorDelayMap();
        }
    }

    /**
     * Handles logging sensor events when a sensor event occurred.
     *
     * @param sensorEvent sensor event
     * @see android.hardware.SensorEvent
     */
    @Override
    public void onSensorChanged(android.hardware.SensorEvent sensorEvent) {
        inputEventDispatcher.dispatch(new SensorEvent(
                sensorTypeMap.getName(sensorEvent.sensor.getType()),
                upTimeNanoseconds + sensorEvent.timestamp,
                inputEventDispatcher.getSessionId(),
                sensorEvent.values)
        );
    }

    /**
     * Handles changes in accuracy of a specific sensor. This method is empty.
     *
     * @param sensor   sensor with changed accuracy
     * @param accuracy new accuracy
     */
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    /**
     * Starts logging sensor events in the specified activity.
     *
     * @param activity    activity to log sensor events in
     * @param sensorNames array of sensor names to register
     * @param sensorDelay delay between sensor measures as string
     */
    public void start(Activity activity, String[] sensorNames, String sensorDelay) {
        registerSensors(activity.getApplicationContext(), sensorNames, sensorDelay);
    }

    /**
     * Stops logging sensor data in the specified activity.
     *
     * @param activity activity to log sensor data in
     */
    public void stop(Activity activity) {
        unregisterAllSensors(activity.getApplicationContext());
    }

    /**
     * Registers the specified sensors by name. Any sensor with invalid name or not available on the
     * current device will be skipped. If {@value #ALL_SENSORS} is specified in the array of
     * sensors, register all sensors.
     *
     * @param context     context to register sensors in
     * @param sensorNames array of sensor names to register
     */
    private void registerSensors(Context context, String[] sensorNames, String delayName) {
        boolean shouldRegisterAllSensors = false;
        Integer delay = sensorDelayMap.getValue(delayName, DEFAULT_SENSOR_DELAY);

        for (String sensorName : sensorNames) {
            if (sensorTypeMap.getProcessedName(sensorName).equals(ALL_SENSORS)) {
                shouldRegisterAllSensors = true;
                break;
            }
        }

        if (!shouldRegisterAllSensors) {
            for (String sensorName : sensorNames) {
                if (sensorTypeMap.containsName(sensorName)) {
                    registerSensor(context, sensorTypeMap.getValue(sensorName), delay);
                }
            }
        } else {
            registerAllSensors(context, delay);
        }

    }

    /**
     * Registers all sensors available on the current device.
     *
     * @param context context to register sensors in
     * @param delay   rate of sensor measurements
     */
    private void registerAllSensors(Context context, int delay) {
        SensorManager manager = getSensorManager(context);
        List<Sensor> sensors = manager.getSensorList(Sensor.TYPE_ALL);

        for (Sensor s : sensors) {
            int type = s.getType();
            manager.registerListener(this, manager.getDefaultSensor(type), delay);
        }
    }

    /**
     * Registers a specific sensor by type.
     *
     * @param context context to register the sensor in
     * @param type    type of sensor to register
     * @param delay   rate of sensor measurements
     */
    private void registerSensor(Context context, int type, int delay) {
        SensorManager manager = getSensorManager(context);
        manager.registerListener(this, manager.getDefaultSensor(type), delay);
    }

    /**
     * Unregisters all available sensors on the current device.
     *
     * @param context context to unregister sensors from. This method has no effect if the
     *                context has no registered sensor listeners.
     */
    private void unregisterAllSensors(Context context) {
        SensorManager manager = getSensorManager(context);
        List<Sensor> sensors = manager.getSensorList(Sensor.TYPE_ALL);

        for (Sensor sensor : sensors) {
            manager.unregisterListener(this, sensor);
        }
    }

    /**
     * Returns a {@link SensorManager} instance containing a set of available sensors.
     *
     * @param context context to get sensor manager from
     * @return
     * @see SensorManager
     */
    private SensorManager getSensorManager(Context context) {
        return (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
    }
}
