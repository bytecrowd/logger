package com.behametrics.logger.InputLoggers.InputEvents;

import com.google.gson.JsonObject;

/**
 * Interface for an event to be logged.
 */
public interface InputEvent {
    String STRING_FIELD_DELIMITER = ";";

    /**
     * Returns the name of the input.
     */
    String getInputName();

    /**
     * Returns a string representation of field names of an input event. If the input type does not
     * support headers, null should be returned.
     */
    String getStringHeader();

    String toCsv();

    JsonObject toJson();
}
