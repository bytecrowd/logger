package com.behametrics.logger.InputLoggers;

import android.os.SystemClock;

import com.behametrics.logger.InputEventDispatcher;
import com.behametrics.logger.InputLoggers.InputEvents.VariableArgEvent;

/**
 * Object providing logging of events with a variable number of arguments.
 */
public class VariableArgEventLogger extends InputLogger {
    private static final long NANOSECONDS_PER_MILLISECOND = 1000000;

    private long upTimeMilliseconds = System.currentTimeMillis() - SystemClock.uptimeMillis();
    private String inputName;
    private boolean loggingEnabled = false;

    /**
     * Creates an instance given the input event dispatcher.
     *
     * @param inputName input name
     * @param inputEventDispatcher input event dispatcher to pass logged events to for further
     *                             processing
     */
    public VariableArgEventLogger(String inputName, InputEventDispatcher inputEventDispatcher) {
        setInputEventDispatcher(inputEventDispatcher);
        this.inputName = inputName;
    }

    /**
     * Enables logging events.
     */
    public void start() {
        loggingEnabled = true;
    }

    /**
     * Disables logging events.
     */
    public void stop() {
        loggingEnabled = false;
    }

    /**
     * Logs an event if logging is enabled (if {@link #start} was called first).
     *
     * @param eventData arbitrary string values representing the event
     * @see #start
     * @see #stop
     */
    public void log(String ... eventData) {
        if (!loggingEnabled) {
            return;
        }

        inputEventDispatcher.dispatch(new VariableArgEvent(
                inputName,
                (upTimeMilliseconds + SystemClock.uptimeMillis()) * NANOSECONDS_PER_MILLISECOND,
                inputEventDispatcher.getSessionId(),
                eventData
        ));
    }

    /**
     * Returns the input name associated with this event logger.
     */
    public String getInputName() {
        return inputName;
    }
}
