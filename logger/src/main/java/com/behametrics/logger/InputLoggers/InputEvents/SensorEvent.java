package com.behametrics.logger.InputLoggers.InputEvents;

import android.support.annotation.NonNull;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Object representing a sensor event (measured from e.g. accelerometer or gyroscope).
 */
public class SensorEvent implements InputEvent {
    public static final String SENSOR_NAME_PREFIX = "sensor_";

    private static final String[] FIXED_SENSOR_FIELDS = new String[]{"inputName", "timestamp", "sessionId"};
    private static final String UNKNOWN_FIELD_PREFIX = "value";
    private static final Map<String, String[]> SENSOR_FIELDS = createSensorFields();

    private String sensorTypeName;
    private long timestamp;
    private String sessionId;
    private float[] values;

    /**
     * Creates a sensor event.
     *
     * @param sensorName name of the sensor providing the measurement
     * @param timestamp  time of measurement in nanoseconds
     * @param sessionId  session ID
     * @param values     array of measured values
     */
    public SensorEvent(String sensorName, long timestamp, String sessionId, float[] values) {
        this.sensorTypeName = sensorName;
        this.timestamp = timestamp;
        this.sessionId = sessionId;
        this.values = values;
    }

    /**
     * Returns the sensor type.
     *
     * @return sensor type
     */
    public String getSensorTypeName() {
        return sensorTypeName;
    }

    @Override
    public String getInputName() {
        return getSensorName(getSensorTypeName());
    }

    @Override
    public String getStringHeader() {
        String[] headerFields = SENSOR_FIELDS.get(getInputName());

        if (headerFields != null) {
            StringBuilder headerBuilder = new StringBuilder();
            String delimiter = "";

            for (String fieldName : headerFields) {
                headerBuilder.append(delimiter);
                delimiter = STRING_FIELD_DELIMITER;
                headerBuilder.append(fieldName);
            }

            return headerBuilder.toString();
        } else {
            return null;
        }
    }

    /**
     * Serializes the event into the comma-separate values (CSV) format.
     *
     * @return string representation of the event, with each field separated by
     * "{@value STRING_FIELD_DELIMITER}"
     */
    @Override
    public String toCsv() {
        return toString(STRING_FIELD_DELIMITER) + "\n";
    }

    /**
     * Serializes the event into the JSON format.
     *
     * @return string representation of the event formatted as JSON
     */
    @Override
    public JsonObject toJson() {
        JsonObject json = new JsonObject();

        json.addProperty("inputName", sensorTypeName);
        json.addProperty("timestamp", timestamp);
        json.addProperty("sessionId", sessionId);

        String[] sensorFields = SENSOR_FIELDS.get(sensorTypeName);
        if (sensorFields == null) {
            sensorFields = new String[0];
        }

        for (int i = 0; i < values.length; i++) {
            String fieldHeader;

            if ((i + FIXED_SENSOR_FIELDS.length) < sensorFields.length) {
                fieldHeader = sensorFields[i + FIXED_SENSOR_FIELDS.length];
            } else {
                fieldHeader = UNKNOWN_FIELD_PREFIX + i;
            }

            json.addProperty(fieldHeader, values[i]);
        }

        return json;
    }

    /**
     * Returns a human-readable representation of the event as delimiter-separated fields.
     *
     * @return string representation of the event
     */
    @NonNull
    @Override
    public String toString() {
        return toString(STRING_FIELD_DELIMITER);
    }

    private String toString(String delimiter) {
        StringBuilder strOutput = new StringBuilder();
        strOutput.append(sensorTypeName)
                .append(delimiter)
                .append(timestamp)
                .append(delimiter)
                .append(sessionId);

        for (float value : values) {
            strOutput.append(delimiter);
            strOutput.append(value);
        }

        return strOutput.toString();
    }

    private static Map<String, String[]> createSensorFields() {
        Map<String, String[]> sensorFields = new HashMap<>();

        sensorFields.put(
                getSensorName("accelerometer"),
                getSensorHeader(new String[]{"accX", "accY", "accZ"}));
        sensorFields.put(
                getSensorName("magnetic_field"),
                getSensorHeader(new String[]{"fieldX", "fieldY", "fieldZ"}));
        sensorFields.put(
                getSensorName("gyroscope"),
                getSensorHeader(new String[]{"angularSpeedX", "angularSpeedY", "angularSpeedZ"}));
        sensorFields.put(
                getSensorName("light"),
                getSensorHeader(new String[]{"lightLevel_Lux"}));
        sensorFields.put(
                getSensorName("pressure"),
                getSensorHeader(new String[]{"pressure_hPa"}));
        sensorFields.put(
                getSensorName("proximity"),
                getSensorHeader(new String[]{"distance_cm"}));
        sensorFields.put(
                getSensorName("gravity"),
                getSensorHeader(new String[]{"gravityX", "gravityY", "gravityZ"}));
        sensorFields.put(
                getSensorName("linear_acceleration"),
                getSensorHeader(new String[]{"accX", "accY", "accZ"}));
        sensorFields.put(
                getSensorName("rotation_vector"),
                getSensorHeader(new String[]{"rotX", "rotY", "rotZ", "magnitude", "accuracy"}));
        sensorFields.put(
                getSensorName("orientation"),
                getSensorHeader(new String[]{"azimuth", "pitch", "roll"}));
        sensorFields.put(
                getSensorName("relative_humidity"),
                getSensorHeader(new String[]{"relativeHumidity_percent"}));
        sensorFields.put(
                getSensorName("ambient_temperature"),
                getSensorHeader(new String[]{"degrees_C"}));
        sensorFields.put(
                getSensorName("magnetic_field_uncalibrated"),
                getSensorHeader(new String[]{
                        "fieldX", "fieldY", "fieldZ", "ironBiasX", "ironBiasY", "ironBiasZ"}));
        sensorFields.put(
                getSensorName("game_rotation_vector"),
                getSensorHeader(new String[]{"rotX", "rotY", "rotZ", "magnitude"}));
        sensorFields.put(
                getSensorName("gyroscope_uncalibrated"),
                getSensorHeader(new String[]{
                        "angularSpeedX", "angularSpeedY", "angularSpeedZ", "driftX", "driftY", "driftZ"}));
        sensorFields.put(
                getSensorName("pose_6dof"),
                getSensorHeader(new String[]{
                        "rotX", "rotY", "rotZ", "magnitude",
                        "translationX", "translationY", "translationZ",
                        "deltaRotX", "deltaRotY", "deltaRotZ", "deltaMagnitude",
                        "deltaTranslationX", "deltaTranslationY", "deltaTranslationZ",
                        "sequenceNumber"
                }));
        sensorFields.put(
                getSensorName("stationary_detect"),
                getSensorHeader(new String[]{"isStationary"}));
        sensorFields.put(
                getSensorName("motion_detect"),
                getSensorHeader(new String[]{"isInMotion"}));
        sensorFields.put(
                getSensorName("heart_beat"),
                getSensorHeader(new String[]{"confidence"}));
        sensorFields.put(
                getSensorName("low_latency_offbody_detect"),
                getSensorHeader(new String[]{"offBodyState"}));
        sensorFields.put(
                getSensorName("accelerometer_uncalibrated"),
                getSensorHeader(new String[]{"accX", "accY", "accZ", "biasX", "biasY", "biasZ"}));

        return sensorFields;
    }

    private static String getSensorName(String sensorTypeName) {
        return SENSOR_NAME_PREFIX + sensorTypeName;
    }

    private static String[] getSensorHeader(String[] baseSensorFields) {
        List<String> headerFields = new ArrayList<>();

        headerFields.addAll(Arrays.asList(FIXED_SENSOR_FIELDS));
        headerFields.addAll(Arrays.asList(baseSensorFields));

        return headerFields.toArray(new String[0]);
    }
}
