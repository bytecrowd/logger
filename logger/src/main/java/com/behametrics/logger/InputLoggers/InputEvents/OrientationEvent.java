package com.behametrics.logger.InputLoggers.InputEvents;

import android.support.annotation.NonNull;

import com.google.gson.JsonObject;

/**
 * Object representing an event describing device orientation.
 */
public class OrientationEvent implements InputEvent {
    public static final String INPUT_NAME = "orientation";

    private static final String[] FIELD_NAMES = {
            "inputName",
            "timestamp",
            "sessionId",
            "orientation"
    };

    private long timestamp;
    private String sessionId;
    private String orientation;

    /**
     * Creates an event describing device orientation.
     *
     * @param timestamp time the event occurred, in nanoseconds
     * @param sessionId session ID
     * @param orientation device orientation
     */
    public OrientationEvent(long timestamp, String sessionId, String orientation) {
        this.timestamp = timestamp;
        this.sessionId = sessionId;
        this.orientation = orientation;
    }

    @Override
    public String getInputName() {
        return INPUT_NAME;
    }

    @Override
    public String getStringHeader() {
        StringBuilder headerBuilder = new StringBuilder();
        String delimiter = "";

        for (String fieldName : FIELD_NAMES) {
            headerBuilder.append(delimiter);
            delimiter = STRING_FIELD_DELIMITER;
            headerBuilder.append(fieldName);
        }

        return headerBuilder.toString();
    }

    /**
     * Serializes the event into the comma-separate values (CSV) format.
     *
     * @return string representation of the event, with each field separated by
     * "{@value STRING_FIELD_DELIMITER}"
     */
    @Override
    public String toCsv() {
        return toString(STRING_FIELD_DELIMITER) + "\n";
    }

    /**
     * Serializes the event into the JSON format.
     *
     * @return string representation of the event formatted as JSON
     */
    @Override
    public JsonObject toJson() {
        JsonObject json = new JsonObject();

        json.addProperty("inputName", INPUT_NAME);
        json.addProperty("timestamp", timestamp);
        json.addProperty("sessionId", sessionId);
        json.addProperty("orientation", orientation);

        return json;
    }

    /**
     * Returns a human-readable representation of the event as delimiter-separated fields.
     *
     * @return string representation of the event
     */
    @NonNull
    @Override
    public String toString() {
        return toString(STRING_FIELD_DELIMITER);
    }

    private String toString(String delimiter) {
        return INPUT_NAME +
                delimiter +
                timestamp +
                delimiter +
                sessionId +
                delimiter +
                orientation;
    }
}
