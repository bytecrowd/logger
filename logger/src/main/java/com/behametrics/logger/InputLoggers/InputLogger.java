package com.behametrics.logger.InputLoggers;

import com.behametrics.logger.InputEventDispatcher;

/**
 * Interface for input loggers.
 */
public abstract class InputLogger {

    protected InputEventDispatcher inputEventDispatcher;

    /**
     * Sets the new input event dispatcher. The value must not be null.
     *
     * @param inputEventDispatcher input event dispatcher
     * @throws IllegalArgumentException if the input event dispatcher is null
     */
    protected void setInputEventDispatcher(InputEventDispatcher inputEventDispatcher) throws IllegalArgumentException {
        if (inputEventDispatcher != null) {
            this.inputEventDispatcher = inputEventDispatcher;
        }
        else {
            throw new IllegalArgumentException();
        }
    }
}
