package com.behametrics.logger.InputLoggers;

import android.app.Activity;
import android.content.ComponentCallbacks;
import android.content.res.Configuration;
import android.os.SystemClock;

import com.behametrics.logger.InputEventDispatcher;
import com.behametrics.logger.InputLoggers.InputEvents.OrientationEvent;
import com.behametrics.logger.Utils.Maps.DeviceOrientationMap;

/**
 * Object logging changes in device orientation.
 */
public class OrientationLogger extends InputLogger implements ComponentCallbacks {
    private static final long NANOSECONDS_PER_MILLISECOND = 1000000;

    private static DeviceOrientationMap deviceOrientationMap;

    private int previousOrientation = -1;
    private long upTimeMilliseconds = System.currentTimeMillis() - SystemClock.uptimeMillis();

    /**
     * Creates a logger logging changes in device orientation given the {@link InputEventDispatcher}
     * instance.
     *
     * @param inputEventDispatcher input event dispatcher to pass logged events to for further
     *                             processing
     */
    public OrientationLogger(InputEventDispatcher inputEventDispatcher) {
        setInputEventDispatcher(inputEventDispatcher);

        if (deviceOrientationMap == null) {
            deviceOrientationMap = new DeviceOrientationMap();
        }
    }

    /**
     * Starts logging events in the specified activity. Consecutive calls to this method without
     * first calling {@link #stop} will have no effect.
     *
     * @param activity activity to log events in
     */
    public void start(Activity activity) {
        logInitialOrientation(activity);
        activity.registerComponentCallbacks(this);
    }

    /**
     * Stops logging events in the specified activity. Calling this method before first calling
     * {@link #start} will have no effect.
     *
     * @param activity activity to stop logging events in
     */
    public void stop(Activity activity) {
        activity.unregisterComponentCallbacks(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.orientation != previousOrientation) {
            inputEventDispatcher.dispatch(new OrientationEvent(
                    (upTimeMilliseconds + SystemClock.uptimeMillis()) * NANOSECONDS_PER_MILLISECOND,
                    inputEventDispatcher.getSessionId(),
                    deviceOrientationMap.getName(newConfig.orientation)
            ));

            previousOrientation = newConfig.orientation;
        }
    }

    @Override
    public void onLowMemory() {
    }

    private void logInitialOrientation(Activity activity) {
        onConfigurationChanged(activity.getResources().getConfiguration());
    }
}
