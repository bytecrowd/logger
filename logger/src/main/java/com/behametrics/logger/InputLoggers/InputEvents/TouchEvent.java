package com.behametrics.logger.InputLoggers.InputEvents;

import com.google.gson.JsonObject;

/**
 * Object representing a touch event.
 */
public class TouchEvent implements InputEvent {
    public static final String INPUT_NAME = "touch";

    private static final String[] FIELD_NAMES = {
            "inputName",
            "timestamp",
            "sessionId",
            "eventTypeName",
            "eventTypeDetail",
            "pointerId",
            "x",
            "y",
            "pressure",
            "size",
            "touchMajor",
            "touchMinor",
            "rawX",
            "rawY"
    };

    private long timestamp;
    private String sessionId;
    private String eventTypeName;
    private String eventTypeDetail;
    private int pointerId;
    private float x;
    private float y;
    private float pressure;
    private float size;
    private float touchMajor;
    private float touchMinor;
    private float rawX;
    private float rawY;

    /**
     * Creates a touch event.
     *
     * @param timestamp       time of measurement in nanoseconds
     * @param sessionId       session ID
     * @param eventTypeName   name of the touch event type (e.g. {@code DOWN})
     * @param eventTypeDetail more specific name for the touch event type
     * @param pointerId       touch pointer ID
     * @param x               touch x-coordinate
     * @param y               touch y-coordinate
     * @param pressure        touch pressure
     * @param size            touch size
     * @param touchMajor      length of the major axis of the touch area
     * @param touchMinor      length of the major axis of the touch area
     * @param rawX            raw touch x-coordinate for the default touch pointer
     * @param rawY            raw touch y-coordinate for the default touch pointer
     */
    public TouchEvent(
            long timestamp,
            String sessionId,
            String eventTypeName,
            String eventTypeDetail,
            int pointerId,
            float x,
            float y,
            float pressure,
            float size,
            float touchMajor,
            float touchMinor,
            float rawX,
            float rawY) {
        this.timestamp = timestamp;
        this.sessionId = sessionId;
        this.eventTypeName = eventTypeName;
        this.eventTypeDetail = eventTypeDetail;
        this.pointerId = pointerId;
        this.x = x;
        this.y = y;
        this.pressure = pressure;
        this.size = size;
        this.touchMajor = touchMajor;
        this.touchMinor = touchMinor;
        this.rawX = rawX;
        this.rawY = rawY;
    }

    @Override
    public String getInputName() {
        return INPUT_NAME;
    }

    @Override
    public String getStringHeader() {
        StringBuilder headerBuilder = new StringBuilder();
        String delimiter = "";

        for (String fieldName : FIELD_NAMES) {
            headerBuilder.append(delimiter);
            delimiter = STRING_FIELD_DELIMITER;
            headerBuilder.append(fieldName);
        }

        return headerBuilder.toString();
    }

    /**
     * Serializes the event into the comma-separate values (CSV) format.
     *
     * @return string representation of the event, with each field separated by
     * "{@value STRING_FIELD_DELIMITER}"
     */
    @Override
    public String toCsv() {
        return toString(STRING_FIELD_DELIMITER) + "\n";
    }

    /**
     * Serializes the event into the JSON format.
     *
     * @return string representation of the event formatted as JSON
     */
    @Override
    public JsonObject toJson() {
        JsonObject json = new JsonObject();

        json.addProperty("inputName", INPUT_NAME);
        json.addProperty("timestamp", timestamp);
        json.addProperty("sessionId", sessionId);
        json.addProperty("eventTypeName", eventTypeName);
        json.addProperty("eventTypeDetail", eventTypeDetail);
        json.addProperty("pointerId", pointerId);
        json.addProperty("x", x);
        json.addProperty("y", y);
        json.addProperty("pressure", pressure);
        json.addProperty("size", size);
        json.addProperty("touchMajor", touchMajor);
        json.addProperty("touchMinor", touchMinor);
        json.addProperty("rawX", rawX);
        json.addProperty("rawY", rawY);

        return json;
    }

    /**
     * Returns a human-readable representation of the event as delimiter-separated fields.
     *
     * @return string representation of the event
     */
    @Override
    public String toString() {
        return toString(STRING_FIELD_DELIMITER);
    }

    private String toString(String delimiter) {
        return INPUT_NAME
                + delimiter
                + timestamp
                + delimiter
                + sessionId
                + delimiter
                + eventTypeName
                + delimiter
                + eventTypeDetail
                + delimiter
                + pointerId
                + delimiter
                + x
                + delimiter
                + y
                + delimiter
                + pressure
                + delimiter
                + size
                + delimiter
                + touchMajor
                + delimiter
                + touchMinor
                + delimiter
                + rawX
                + delimiter
                + rawY;
    }
}
