package com.behametrics.logger.InputLoggers.InputEvents;

import android.support.annotation.NonNull;

import com.google.gson.JsonObject;

/**
 * Object representing an event with a variable number of arguments.
 */
public class VariableArgEvent implements InputEvent {
    private String inputName;
    private long timestamp;
    private String sessionId;
    private String[] eventData;

    /**
     * Creates an event with a variable number of arguments.
     *
     * @param inputName input name
     * @param timestamp time the event occurred, in nanoseconds
     * @param sessionId session ID
     * @param eventData arbitrary strings representing the event
     */
    public VariableArgEvent(String inputName, long timestamp, String sessionId, String[] eventData) {
        this.inputName = inputName;
        this.timestamp = timestamp;
        this.sessionId = sessionId;
        this.eventData = eventData;
    }

    @Override
    public String getInputName() {
        return inputName;
    }

    @Override
    public String getStringHeader() {
        return null;
    }

    /**
     * Serializes the event into the comma-separate values (CSV) format.
     *
     * @return string representation of the event, with each field separated by
     * "{@value STRING_FIELD_DELIMITER}"
     */
    @Override
    public String toCsv() {
        return toString(STRING_FIELD_DELIMITER) + "\n";
    }

    /**
     * Serializes the event into the JSON format.
     *
     * @return string representation of the event formatted as JSON
     */
    @Override
    public JsonObject toJson() {
        JsonObject json = new JsonObject();

        json.addProperty("inputName", inputName);
        json.addProperty("timestamp", timestamp);
        json.addProperty("sessionId", sessionId);

        for (int i = 0; i < eventData.length; i++) {
            json.addProperty("value" + i, eventData[i]);
        }

        return json;
    }

    /**
     * Returns a human-readable representation of the event as delimiter-separated fields.
     *
     * @return string representation of the event
     */
    @NonNull
    @Override
    public String toString() {
        return toString(STRING_FIELD_DELIMITER);
    }

    private String toString(String delimiter) {
        StringBuilder strOutput = new StringBuilder();
        strOutput.append(inputName)
                .append(delimiter)
                .append(timestamp)
                .append(delimiter)
                .append(sessionId);

        for (String eventDataElement : eventData) {
            strOutput.append(delimiter);
            strOutput.append(eventDataElement);
        }

        return strOutput.toString();
    }
}
