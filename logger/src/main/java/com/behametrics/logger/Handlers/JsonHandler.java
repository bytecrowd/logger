package com.behametrics.logger.Handlers;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.util.Log;

import com.behametrics.logger.InputLoggers.InputEvents.InputEvent;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Object providing sending logged events as JSON objects to the specified server.
 *
 * <p>JSON objects are sent in batches in a separate thread. The batch is sent if full or after a
 * time period of {@value #LESS_THAN_BATCH_SIZE_SEND_DELAY_MILLISECONDS} milliseconds when no new
 * JSON object was added to the batch.</p>
 */
public class JsonHandler implements InputEventHandler {

    private static final int INPUT_EVENT_BATCH_SIZE_TO_SEND = 100;
    private static final int LESS_THAN_BATCH_SIZE_SEND_DELAY_MILLISECONDS = 500;

    private InputEventSendService service;
    private InputEventHandlerThread inputEventHandlerThread;

    /**
     * Create a new JSON handler.
     *
     * @param serverUrl server URL formatted as {@code http[s]://&lt;address&gt;[:&lt;port&gt;]}
     * @param endpointUrl endpoint URL relative to the server URL
     * @param loggerHeader contents of the {@code Logger} HTTP header added to each batch of JSON
     *                     objects to be sent
     */
    public JsonHandler(@NonNull String serverUrl, @NonNull String endpointUrl, @NonNull String loggerHeader) {
        Retrofit retrofit = null;

        try {
            retrofit = new Retrofit.Builder()
                    .baseUrl(serverUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        } catch(IllegalArgumentException e) {
            Log.e("JsonHandler", "Could not initialize handler: " + e);
        }

        if (retrofit != null) {
            service = retrofit.create(InputEventSendService.class);

            Map<String, String> headers = new LinkedHashMap<>();
            headers.put("Content-type", "application/json");
            headers.put("Logger", loggerHeader);

            inputEventHandlerThread = new InputEventHandlerThread(
                    "InputEventHandlerThread",
                    endpointUrl,
                    service,
                    headers,
                    INPUT_EVENT_BATCH_SIZE_TO_SEND,
                    LESS_THAN_BATCH_SIZE_SEND_DELAY_MILLISECONDS);
            inputEventHandlerThread.start();
        }
    }

    /**
     * Sends logged input event in JSON format.
     *
     * @param inputEvent input event to send
     * @return true if sending was successful, false otherwise
     */
    @Override
    public boolean handle(InputEvent inputEvent) {
        if (service == null) {
            return false;
        }

        inputEventHandlerThread.sendInputEvent(inputEvent);

        return true;
    }
}

interface InputEventSendService {

    @POST("/{path}")
    Call<ResponseBody> sendInputEvent(
            @Path("path") String endpointRelativeUrl,
            @HeaderMap Map<String, String> headers,
            @Body List<JsonObject> inputEventJsonObjects);
}

/**
 * {@code HandlerThread} class sending input events as JSON objects in batches.
 *
 * <p>The thread also accepts messages to schedule sending JSON objects explicitly if the handler
 * did not send send any objects after a specified period of time.</p>
 */
class InputEventHandlerThread extends HandlerThread {

    private static final int MESSAGE_ADD_OBJECT_OR_SEND_OBJECTS = 1;
    private static final int MESSAGE_SCHEDULE_SENDING_OBJECTS = 2;

    private String endpointUrl;
    private InputEventSendService service;
    private Map<String, String> headers;
    private int batchSize;
    private int lessThanBatchSizeSendDelayMilliseconds;

    private Handler inputEventMessageHandler;
    private List<JsonObject> inputEventJsonObjects;
    private Timer timer;
    private long upTimeMilliseconds;

    public InputEventHandlerThread(
            String name,
            String endpointUrl,
            InputEventSendService service,
            Map<String, String> headers,
            int batchSize,
            int lessThanBatchSizeSendDelayMilliseconds) {
        super(name);

        this.endpointUrl = endpointUrl;
        this.service = service;
        this.headers = headers;
        this.batchSize = batchSize;
        this.lessThanBatchSizeSendDelayMilliseconds = lessThanBatchSizeSendDelayMilliseconds;

        inputEventJsonObjects = new ArrayList<>(batchSize);
    }

    public void sendInputEvent(InputEvent inputEvent) {
        Message message = new Message();
        message.obj = inputEvent;
        message.what = MESSAGE_ADD_OBJECT_OR_SEND_OBJECTS;

        inputEventMessageHandler.sendMessage(message);
    }

    public void scheduleSendingInputEvents() {
        Message message = new Message();
        message.what = MESSAGE_SCHEDULE_SENDING_OBJECTS;

        inputEventMessageHandler.sendMessage(message);
    }

    @Override
    protected void onLooperPrepared() {
        super.onLooperPrepared();

        inputEventMessageHandler = new Handler(getLooper()) {
            @Override
            public void handleMessage(Message message) {
                super.handleMessage(message);

                long newUpTimeMilliseconds = SystemClock.uptimeMillis();
                long elapsedTimeMilliseconds = newUpTimeMilliseconds - upTimeMilliseconds;

                switch (message.what) {
                    case MESSAGE_ADD_OBJECT_OR_SEND_OBJECTS:
                        addOrSendInputEvents((InputEvent)message.obj);
                        break;
                    case MESSAGE_SCHEDULE_SENDING_OBJECTS:
                        if (elapsedTimeMilliseconds > lessThanBatchSizeSendDelayMilliseconds
                                && !inputEventJsonObjects.isEmpty()) {
                            sendInputEvents();
                        }
                        break;
                    default:
                        break;
                }

                upTimeMilliseconds = newUpTimeMilliseconds;
            }
        };

        upTimeMilliseconds = SystemClock.uptimeMillis();

        setScheduledSendingTimer();
    }

    private void addOrSendInputEvents(InputEvent inputEvent) {
        if (inputEventJsonObjects.size() < batchSize) {
            inputEventJsonObjects.add(inputEvent.toJson());
        } else {
            sendInputEvents();
        }
    }

    private void sendInputEvents() {
        sendJsonObjects();
        inputEventJsonObjects.clear();
    }

    private void sendJsonObjects() {
        Call<ResponseBody> call = service.sendInputEvent(endpointUrl, headers, inputEventJsonObjects);
        call.enqueue(new EmptyCallback());
    }

    private void setScheduledSendingTimer() {
        timer = new Timer();
        timer.schedule(new TimerTask() {
                           @Override
                           public void run() {
                               scheduleSendingInputEvents();
                           }
                       },
                0,
                lessThanBatchSizeSendDelayMilliseconds);
    }
}

class EmptyCallback implements Callback<ResponseBody> {

    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
    }
}
