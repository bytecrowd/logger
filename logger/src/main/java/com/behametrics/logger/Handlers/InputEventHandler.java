package com.behametrics.logger.Handlers;

import com.behametrics.logger.InputLoggers.InputEvents.InputEvent;

/**
 * Interface for handling input events for further processing. Classes implementing this interface
 * may, for example, persistently store the logged event or send the event over the network.
 */
public interface InputEventHandler {
    boolean handle(InputEvent inputEvent);
}
