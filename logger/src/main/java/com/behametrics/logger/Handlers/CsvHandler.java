package com.behametrics.logger.Handlers;

import android.util.Log;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.behametrics.logger.InputLoggers.InputEvents.InputEvent;
import com.behametrics.logger.Utils.Properties.PropertyReader;

/**
 * Object providing writing logged data to files in a comma-separated values (CSV) format.
 */
public class CsvHandler implements Closeable, InputEventHandler {
    private static final int BYTES_PER_MB = 1024 * 1024;

    private Map<String, BufferedWriter> bufferedWriters;
    private String logsDirpath;
    private int minFreeSpaceLimitMB;
    private boolean debugLogs;

    /**
     * Creates a {@link CsvHandler} instance given the root directory and the property reader to
     * read configuration values from.
     *
     * @param logsDirpath         root directory to write logged input events in
     * @param minFreeSpaceLimitMB minimum free storage space in megabytes
     * @param debugLogs           true to display logged events in the console, false otherwise.
     *                            Applies to debug builds only (release builds have all debug logs
     *                            disabled).
     * @throws OutOfMemoryError if there is not enough space to store logged data
     */
    public CsvHandler(String logsDirpath, int minFreeSpaceLimitMB, boolean debugLogs) throws OutOfMemoryError {
        this.logsDirpath = logsDirpath;
        this.minFreeSpaceLimitMB = minFreeSpaceLimitMB;
        this.debugLogs = debugLogs;

        this.bufferedWriters = new HashMap<>();
    }


    public CsvHandler(String logsDirpath, int minFreeSpaceLimitMB) throws OutOfMemoryError {
        this(logsDirpath, minFreeSpaceLimitMB, false);
    }

    /**
     * Writes a serialized event to a file for the corresponding input.
     *
     * <p>
     * The file name is determined by the input name. If the file did not exist prior to calling
     * this method, the first line will be a header describing each field of the input events.
     * </p>
     *
     * @param inputEvent input event to serialize and write
     * @return true if the event was written successfully, false otherwise
     */
    @Override
    public boolean handle(InputEvent inputEvent) {
        try {
            write(inputEvent);
        } catch (IOException | OutOfMemoryError e) {
            Log.e("CsvHandler", "Error writing to file: ", e);
            return false;
        }
        return true;
    }

    private void write(InputEvent inputEvent) throws IOException, OutOfMemoryError {
        if (!bufferedWriters.containsKey(inputEvent.getInputName())) {
            createLogFile(inputEvent.getInputName(), inputEvent.getStringHeader());
        }

        BufferedWriter bufferedWriter = bufferedWriters.get(inputEvent.getInputName());
        if (bufferedWriter != null) {
            String serializedInputEvent = inputEvent.toCsv();

            if (debugLogs) {
                Log.d("CsvHandler", serializedInputEvent);
            }

            bufferedWriter.write(serializedInputEvent);
            bufferedWriter.flush();
        }
    }

    /**
     * Closes all opened files for logging.
     *
     * @throws IOException if closing any of the files failed
     */
    @Override
    public void close() throws IOException {
        for (BufferedWriter bufferedWriter : bufferedWriters.values()) {
            bufferedWriter.close();
        }

        bufferedWriters.clear();
    }

    /**
     * Creates a new file for storing logged events given the name of the input providing the
     * events.
     *
     * @param filename filename of the log file
     * @param header   first line of the log file. Passing null will skip the header.
     * @throws IOException      if creating the log file failed
     * @throws OutOfMemoryError if there is not enough space to store logged events
     */
    private void createLogFile(String filename, String header) throws IOException, OutOfMemoryError {
        createLogsDirectory();

        String logFilepath = getLogFilepath(filename);
        boolean shouldWriteHeader = fileExists(logFilepath);
        BufferedWriter bufferedWriter;

        if (getUsableSpaceMB() > minFreeSpaceLimitMB) {
            bufferedWriter = getNewLogFile(logFilepath);
        } else {
            throw new OutOfMemoryError("Free space limit is less than " + minFreeSpaceLimitMB + "MB");
        }

        if (shouldWriteHeader && header != null) {
            if (debugLogs) {
                Log.d("CsvHandler", filename + ": Writing header " + header);
            }

            bufferedWriter.write(header + "\n");
        }

        bufferedWriters.put(filename, bufferedWriter);
    }

    private long getUsableSpaceMB() {
        return new File(logsDirpath).getUsableSpace() / BYTES_PER_MB;
    }

    private boolean createLogsDirectory() {
        boolean directoriesCreated = new File(logsDirpath).mkdirs();

        if (!directoriesCreated) {
            Log.d("CsvHandler", "Failed to create directory " + logsDirpath);
        }

        return directoriesCreated;
    }

    private BufferedWriter getNewLogFile(String logFilepath) throws IOException {
        return new BufferedWriter(new FileWriter(logFilepath, true));
    }

    private String getLogFilepath(String inputName) {
        return logsDirpath + "/" + inputName + ".csv";
    }

    private boolean fileExists(String logFilepath) {
        return !new File(logFilepath).exists();
    }
}
