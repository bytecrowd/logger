package com.behametrics.logger;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import com.behametrics.logger.Handlers.InputEventHandler;
import com.behametrics.logger.Handlers.JsonHandler;
import com.behametrics.logger.Handlers.CsvHandler;
import com.behametrics.logger.InputLoggers.InputEvents.InputEvent;
import com.behametrics.logger.Utils.Properties.PropertyReader;

/**
 * Object dispatching logged events to appropriate handlers.
 */
public class InputEventDispatcher {

    private CsvHandler csvHandler;
    private List<InputEventHandler> handlers;
    private String sessionId;

    /**
     * Creates an input event dispatcher.
     *
     * @param rootDirpath
     * @param sessionId      a unique identifier describing a session. The identifier can be used to
     *                       distinguish devices, different versions of the application or usage
     *                       sessions of the application.
     * @param propertyReader
     */
    InputEventDispatcher(String rootDirpath, String sessionId, PropertyReader propertyReader) {
        setSessionId(sessionId);

        try {
            csvHandler = new CsvHandler(
                    rootDirpath + "/" + propertyReader.getStringProperty("LogsDirpath"),
                    propertyReader.getIntProperty("MinFreeSpaceLimitMB"),
                    propertyReader.getBooleanProperty("DebugLogs"));
        } catch (OutOfMemoryError e) {
            Log.e("InputEventDispatcher", e.toString());
        }

        handlers = new ArrayList<>();
        handlers.add(csvHandler);

        String serverUrl = propertyReader.getStringProperty("ServerUrl");
        if (!serverUrl.isEmpty()) {
            handlers.add(
                    new JsonHandler(
                            serverUrl,
                            propertyReader.getStringProperty("EndpointUrl"),
                            propertyReader.getStringProperty("LoggerHeader")));
        }
    }

    /**
     * Passes the specified input event to handlers that are responsible for e.g. storing the event
     * to a file or sending the event over the network.
     *
     * @param inputEvent input event to process
     */
    public void dispatch(InputEvent inputEvent) {
        for (InputEventHandler handler : handlers) {
            handler.handle(inputEvent);
        }
    }

    /**
     * Returns the current session ID.
     *
     * @return session ID
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Sets a new session ID.
     *
     * @param sessionId new session ID
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
