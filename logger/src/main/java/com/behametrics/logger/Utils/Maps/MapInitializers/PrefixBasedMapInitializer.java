package com.behametrics.logger.Utils.Maps.MapInitializers;

import com.behametrics.logger.Utils.Maps.MemberNameValueMap;

import java.lang.reflect.Field;

/**
 * Class initializing {@link MemberNameValueMap} instances with static class members of the same
 * prefix (e.g. static members of {@link android.hardware.Sensor} whose names start with
 * {@code TYPE_*}).
 *
 * <p>
 * If one or more members share the same value, the member name processed last will be mapped to the
 * value.
 * </p>
 */
public class PrefixBasedMapInitializer<T> implements MapInitializer<T> {
    private String prefix;

    /**
     * Instantiate the initializer given the string prefix matching static member names of a class.
     *
     * @param prefix string prefix for static class member names
     */
    public PrefixBasedMapInitializer(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public void initMap(MemberNameValueMap<T> map, Class classWithFields) {
        for (Field field : classWithFields.getFields()) {
            if (field.getName().startsWith(prefix)) {
                String fieldName = getFieldName(map, field);
                T fieldValue = map.getStaticMemberValue(field);

                if (fieldValue != null) {
                    map.put(fieldName, fieldValue);
                }
            }
        }
    }

    private String getFieldName(MemberNameValueMap<T> map, Field field) {
        return map.getProcessedName(field.getName().substring(prefix.length()));
    }
}
