package com.behametrics.logger.Utils.Maps;

import com.behametrics.logger.Utils.Maps.MapInitializers.MapInitializer;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;

/**
 * Class constructing a map of static member names to their values and vice versa for specific
 * class(es). All static member values should be of the same type.
 *
 * <p>
 * A {@link MapInitializer} instance fills the map contents at the time of instantiation. The
 * initializer is also responsible for handling multiple member names mapping to the same value.
 * </p>
 */
public class MemberNameValueMap<T> {
    private LinkedHashMap<String, T> nameValueMap;
    private LinkedHashMap<T, String> valueNameMap;

    /**
     * Creates a map given the map initializer.
     *
     * @param mapInitializer initializer that fills the map entries, or null to skip initialization
     */
    public MemberNameValueMap(MapInitializer<T> mapInitializer, Class classWithFields) {
        nameValueMap = new LinkedHashMap<>();
        valueNameMap = new LinkedHashMap<>();

        if (mapInitializer != null) {
            mapInitializer.initMap(this, classWithFields);
        }
    }

    /**
     * Returns member value given the member name. If the name does not exist, null is returned.
     *
     * @param name member name
     * @return member value or null if the member name is not valid
     */
    public T getValue(String name) {
        return getValue(name, null);
    }

    /**
     * Returns member value given the member name. If the name does not exist, the specified default
     * value is returned.
     *
     * @param name         member name
     * @param defaultValue default value
     * @return member value or the default value if the member name is not valid
     */
    public T getValue(String name, T defaultValue) {
        T value = nameValueMap.get(getProcessedName(name));
        if (value != null) {
            return value;
        } else {
            return defaultValue;
        }
    }

    /**
     * Returns true if the member name exists in the map, false otherwise.
     *
     * @param name member name
     * @return
     */
    public boolean containsName(String name) {
        return nameValueMap.containsKey(getProcessedName(name));
    }

    /**
     * Returns member value for the given member name. If the value does not exist, null is
     * returned.
     *
     * @param value member value
     * @return member name or null if the member value is not valid
     */
    public String getName(T value) {
        return getName(value, null);
    }

    /**
     * Returns member value for the given member name. If the value does not exist, the specified
     * default name is returned.
     *
     * @param value       member value
     * @param defaultName default member name
     * @return member name or the default name if the member value is not valid
     */
    public String getName(T value, String defaultName) {
        String name = valueNameMap.get(value);
        if (name != null) {
            return name;
        } else {
            return defaultName;
        }
    }

    /**
     * Returns true if the value exists in the map, false otherwise.
     *
     * @param value member value
     * @return
     */
    public boolean containsValue(T value) {
        return valueNameMap.containsKey(value);
    }

     /**
     * Adds an entry given the member name and its value.
     *
     * @param name member name
     * @param value member value
     */
    public void put(String name, T value) {
        nameValueMap.put(name, value);
        valueNameMap.put(value, name);
    }

    /**
     * Returns a processed member name, invoked before accessing the name in the map. This method
     * returns the member name in lowercase. Subclasses may override this method to apply additional
     * or different processing to the member name.
     *
     * @param name unprocessed member name
     * @return member name in lowercase
     */
    public String getProcessedName(String name) {
        return name.toLowerCase();
    }

    /**
     * Returns the value of a static member.
     *
     * @param field field to get static value from
     * @return static member value, or null if the field is not static or otherwise valid
     */
    @SuppressWarnings("unchecked")
    public T getStaticMemberValue(Field field) {
        try {
            return (T) field.get(null);
        } catch (ClassCastException | IllegalAccessException | IllegalArgumentException | NullPointerException e) {
            return null;
        }
    }
}
