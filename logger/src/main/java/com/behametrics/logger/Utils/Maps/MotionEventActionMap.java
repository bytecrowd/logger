package com.behametrics.logger.Utils.Maps;

import android.view.MotionEvent;

import com.behametrics.logger.Utils.Maps.MapInitializers.PrefixBasedMapInitializer;

/**
 * A map of sensor delay names to sensor delay types and vice versa according to
 * {@link MotionEvent}. All members of {@link MotionEvent} prefixed with
 * {@value #MEMBER_NAME_PREFIX} are included in the map.
 */
public class MotionEventActionMap extends MemberNameValueMap<Integer> {
    private static final String MEMBER_NAME_PREFIX = "ACTION_";

    public MotionEventActionMap() {
        super(new PrefixBasedMapInitializer<Integer>(MEMBER_NAME_PREFIX), MotionEvent.class);
    }
}
