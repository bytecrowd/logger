package com.behametrics.logger.Utils.Maps;

import android.content.res.Configuration;

import com.behametrics.logger.Utils.Maps.MapInitializers.PrefixBasedMapInitializer;

/**
 * A map of sensor delay names to sensor delay types and vice versa according to
 * {@link Configuration}. All members of {@link Configuration} prefixed with
 * {@value #MEMBER_NAME_PREFIX} are included in the map.
 */
public class DeviceOrientationMap extends MemberNameValueMap<Integer> {
    private static final String MEMBER_NAME_PREFIX = "ORIENTATION_";

    public DeviceOrientationMap() {
        super(new PrefixBasedMapInitializer<Integer>(MEMBER_NAME_PREFIX), Configuration.class);
    }
}
