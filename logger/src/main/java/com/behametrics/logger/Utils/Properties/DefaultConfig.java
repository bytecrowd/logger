package com.behametrics.logger.Utils.Properties;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains default configuration values for the library.
 */
class DefaultConfig {

    private Map<String, String> config;

    DefaultConfig() {
        config = new HashMap<>();
        config.put("Inputs", "touch,orientation,custom,sensor_accelerometer,sensor_gyroscope");
        config.put("SensorLogRate", "game");
        config.put("StorageType", "internal");
        config.put("LogsDirpath", "logs");
        config.put("MinFreeSpaceLimitMB", "300");
        config.put("DebugLogs", "false");
        config.put("ServerUrl", "");
        config.put("EndpointUrl", "data");
        config.put("LoggerHeader", "fastar/android-v2");
        config.put("AuthSocketUrl", "");
    }

    Map<String, String> getConfig() {
        return config;
    }
}
