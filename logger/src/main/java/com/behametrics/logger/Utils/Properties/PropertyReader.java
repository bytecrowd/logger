package com.behametrics.logger.Utils.Properties;

import android.content.Context;
import android.content.res.AssetManager;
import android.support.annotation.NonNull;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.regex.Pattern;

/**
 * Object providing convenience methods to parse entries from {@link java.util.Properties}
 * instances and creating copies of properties. Upon instantiation, the object contains default
 * values for configuration entries for the library.
 */
public class PropertyReader {
    private Properties properties;

    /**
     * Creates a property reader filled with default configuration values.
     */
    public PropertyReader() {
        properties = new Properties();
        properties.putAll(new DefaultConfig().getConfig());
    }

    /**
     * Sets a property.
     *
     * @param name  property name
     * @param value property value
     */
    public void setProperty(String name, String value) {
        properties.setProperty(name, value);
    }

    /**
     * Returns the underlying {@link java.util.Properties} instance.
     *
     * @return
     */
    public Properties getProperties() {
        return properties;
    }

    /**
     * Loads properties from a file stored in the {@code assets} folder. Any existing property will
     * be updated with the value from the new properties.
     *
     * @param context  context to get {@link AssetManager} instance from
     * @param fileName asset file name containing properties
     * @return true if properties were loaded successfully, false otherwise (failed to find or open
     * the asset file)
     */
    public boolean loadProperties(Context context, String fileName) {
        AssetManager assetManager = context.getAssets();

        try {
            properties.load(assetManager.open(fileName));
        } catch (IOException e) {
            return false;
        }

        return true;
    }

    /**
     * Returns a property as a string. Returns an empty string if the key does not exist.
     *
     * @param key property key
     * @return property value
     */
    @NonNull
    public String getStringProperty(String key) {
        String property = properties.getProperty(key);
        return property != null ? property : "";
    }

    /**
     * Returns a property as an integer. Returns 0 if the key does not exist or the property cannot
     * be converted to an integer.
     *
     * @param key property key
     * @return property value as integer
     */
    public int getIntProperty(String key) {
        String property = properties.getProperty(key);

        if (property != null) {
            try {
                return Integer.parseInt(property);
            } catch (NumberFormatException e) {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Returns a property as a boolean. {@code "True"} (case-insensitive) is converted to true, any
     * other value is converted to false.
     *
     * @param key property key
     * @return property value as a boolean
     */
    public boolean getBooleanProperty(String key) {
        return properties.getProperty(key) != null && Boolean.parseBoolean(properties.getProperty(key));
    }

    /**
     * Returns a property as a list of strings extracted from a string containing the specified
     * separator.
     *
     * @param key       property key
     * @param separator string separating the substrings
     * @return list of strings
     */
    @NonNull
    public String[] getStringArrayProperty(String key, String separator) {
        return getStringArrayProperty(key, separator, false);
    }

    /**
     * Returns a property as a list of strings extracted from a string containing the specified
     * separator. Optionally allows trimming whitespace.
     *
     * @param key       property key
     * @param separator string separating the substrings
     * @param trimWhitespace if true, remove whitespace characters from the beginning and end of
     *                       each element
     * @return list of strings
     */
    @NonNull
    public String[] getStringArrayProperty(String key, String separator, boolean trimWhitespace) {
        String property = properties.getProperty(key);

        if (property != null) {
            if (separator != null && !separator.isEmpty()) {
                String[] elements = property.split(Pattern.quote(separator));

                if (trimWhitespace) {
                    for (int i = 0; i < elements.length; i++) {
                        elements[i] = elements[i].trim();
                    }
                }

                return elements;
            } else {
                return new String[]{property};
            }
        } else {
            return new String[]{};
        }
    }

    /**
     * Returns a copy of the properties in a new {@link PropertyReader} instance.
     *
     * @return new {@link PropertyReader} instance
     */
    public PropertyReader copy() {
        PropertyReader newPropertyReader = new PropertyReader();
        Enumeration p = properties.propertyNames();

        while (p.hasMoreElements()) {
            String key = (String) p.nextElement();
            newPropertyReader.setProperty(key, properties.getProperty(key));
        }

        return newPropertyReader;
    }
}
