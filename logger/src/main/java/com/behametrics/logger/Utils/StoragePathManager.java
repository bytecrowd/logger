package com.behametrics.logger.Utils;

import android.content.Context;

import java.io.File;

/**
 * Class providing a path to store logged data given the specified storage type.
 */
public class StoragePathManager {
    private static final String EXTERNAL_NUMBER_PREFIX = "external_";

    /**
     * Returns a directory path to store logged data given the specified storage type.
     *
     * @param context context to get directory path from
     * @param storageType storage type. Possible values:
     * <ul>
     *   <li>"internal" (default) - internal storage<li/>
     *   <li>"external" (default) - the primary external storage<li/>
     *   <li>"external_[number]" (default) - [number]-th external storage (starting from 0). If that
     *   storage is not available, the first (primary) external storage is used.<li/>
     * <ul/>
     * <p>Other values are interpreted as `internal`. If the external storage is not available, the
     * internal storage is used.
     * <p/>
     * @return storage directory path
     */
    public static String getStorageDirpath(Context context, String storageType) {
        if (storageType == null) {
            return context.getFilesDir().getAbsolutePath();
        }

        if ("internal".equals(storageType)) {
            return context.getFilesDir().getAbsolutePath();
        } else if ("external".equals(storageType)) {
            String externalStorageDirpath = getExternalStorageDirpath(context);
            if (externalStorageDirpath != null) {
                return externalStorageDirpath;
            }
        } else if (storageType.startsWith(EXTERNAL_NUMBER_PREFIX)) {
            String externalStorageNumberStr = storageType.substring(EXTERNAL_NUMBER_PREFIX.length());
            Integer externalStorageNumber = null;
            try {
                externalStorageNumber = Integer.parseInt(externalStorageNumberStr);
            } catch (NumberFormatException e) {
            }

            if (externalStorageNumber != null) {
                String externalStorageDirpath = getExternalStorageDirpath(context, externalStorageNumber);
                if (externalStorageDirpath != null) {
                    return externalStorageDirpath;
                }
            }
        }

        return context.getFilesDir().getAbsolutePath();
    }

    private static String getExternalStorageDirpath(Context context) {
        File externalStorageFile = context.getExternalFilesDir(null);
        if (externalStorageFile != null) {
            return externalStorageFile.getAbsolutePath();
        } else {
            return null;
        }
    }

    private static String getExternalStorageDirpath(Context context, int externalStorageIndex) {
        File[] externalStorageFiles = context.getExternalFilesDirs(null);
        if (externalStorageFiles != null) {
            if (externalStorageIndex < externalStorageFiles.length) {
                File externalStorageFile = null;
                try {
                    externalStorageFile = externalStorageFiles[externalStorageIndex];
                } catch (ArrayIndexOutOfBoundsException e) {
                }

                if (externalStorageFile != null) {
                    return externalStorageFile.getAbsolutePath();
                } else {
                    return getExternalStorageDirpath(context);
                }
            } else {
                return getExternalStorageDirpath(context);
            }
        }

        return null;
    }
}
