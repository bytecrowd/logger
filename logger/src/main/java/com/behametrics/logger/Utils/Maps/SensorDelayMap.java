package com.behametrics.logger.Utils.Maps;

import android.hardware.SensorManager;

import com.behametrics.logger.Utils.Maps.MapInitializers.PrefixBasedMapInitializer;

/**
 * A map of sensor delay names to sensor delay types and vice versa according to
 * {@link SensorManager}. All members of {@link SensorManager} prefixed with
 * {@value #MEMBER_NAME_PREFIX} are included in the map.
 */
public class SensorDelayMap extends MemberNameValueMap<Integer> {
    private static final String MEMBER_NAME_PREFIX = "SENSOR_DELAY_";

    public SensorDelayMap() {
        super(new PrefixBasedMapInitializer<Integer>(MEMBER_NAME_PREFIX), SensorManager.class);
    }
}
