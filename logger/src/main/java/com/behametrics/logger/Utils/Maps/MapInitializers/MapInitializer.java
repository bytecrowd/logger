package com.behametrics.logger.Utils.Maps.MapInitializers;

import com.behametrics.logger.Utils.Maps.MemberNameValueMap;

/**
 * Interface for initializing {@link MemberNameValueMap} instances.
 *
 * @param <T> type of static class members to be filled in the map
 */
public interface MapInitializer<T> {
    /**
     * Initialize a {@link MemberNameValueMap} instance given the class to retrieve static class
     * member names and values from. Each specific initializer should choose which member names and
     * values will be put into the map.
     *
     * @param map map to fill
     * @param classWithFields class to get static member names and values from
     */
    void initMap(MemberNameValueMap<T> map, Class classWithFields);
}
