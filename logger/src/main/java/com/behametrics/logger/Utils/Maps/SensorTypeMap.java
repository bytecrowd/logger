package com.behametrics.logger.Utils.Maps;

import android.hardware.Sensor;

import com.behametrics.logger.Utils.Maps.MapInitializers.PrefixBasedMapInitializer;

/**
 * A map of sensor delay names to sensor delay types and vice versa according to
 * {@link Sensor}. All members of {@link Sensor} prefixed with
 * {@value #MEMBER_NAME_PREFIX} are included in the map.
 *
 * <p>
 * Note that since the map is generated dynamically, some members of {@link Sensor} prefixed with
 * {@value #MEMBER_NAME_PREFIX} are not actual sensors (such as
 * {@link Sensor#TYPE_DEVICE_PRIVATE_BASE}).
 * </p>
 */
public class SensorTypeMap extends MemberNameValueMap<Integer> {
    private static final String MEMBER_NAME_PREFIX = "TYPE_";

    public SensorTypeMap() {
        super(new PrefixBasedMapInitializer<Integer>(MEMBER_NAME_PREFIX), Sensor.class);
    }
}
