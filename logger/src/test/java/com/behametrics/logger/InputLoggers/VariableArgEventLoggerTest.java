package com.behametrics.logger.InputLoggers;

import com.behametrics.logger.InputEventDispatcher;
import com.behametrics.logger.InputLoggers.InputEvents.InputEvent;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

public class VariableArgEventLoggerTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    private VariableArgEventLogger variableArgEventLogger;

    @Mock
    private InputEventDispatcher mockedInputEventDispatcher;

    @Before
    public void setUp() {
        variableArgEventLogger = new VariableArgEventLogger("custom", mockedInputEventDispatcher);
    }

    @Test
    public void logEventAfterStart() {
        variableArgEventLogger.start();
        variableArgEventLogger.log("start_of_activity");
        Mockito.verify(
                mockedInputEventDispatcher,
                Mockito.times(1)).dispatch(Mockito.any(InputEvent.class));
    }

    @Test
    public void logNoEventWithoutStart() {
        variableArgEventLogger.log("start_of_activity");
        Mockito.verify(
                mockedInputEventDispatcher,
                Mockito.times(0)).dispatch(Mockito.any(InputEvent.class));
    }

    @Test
    public void logNoEventAfterStartAndStop() {
        variableArgEventLogger.start();
        variableArgEventLogger.stop();
        variableArgEventLogger.log("start_of_activity");
        Mockito.verify(
                mockedInputEventDispatcher,
                Mockito.times(0)).dispatch(Mockito.any(InputEvent.class));
    }
}