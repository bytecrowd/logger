package com.behametrics.logger.Utils;

import android.hardware.Sensor;

import com.behametrics.logger.Utils.Maps.SensorTypeMap;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

@RunWith(JUnitParamsRunner.class)
public class SensorTypeMapTest {

    private static SensorTypeMap sensorTypeMap;

    @BeforeClass
    public static void setUpClass() {
        sensorTypeMap = new SensorTypeMap();
    }

    @Test
    @Parameters(method = "getValueParams")
    public void getValue(String sensorName, Integer expectedSensorType) {
        assertThat(sensorTypeMap.getValue(sensorName), is(expectedSensorType));
    }
    private Object getValueParams() {
        return new Object[] {
                new Object[] {"accelerometer", Sensor.TYPE_ACCELEROMETER},
                new Object[] {"ACCELEROMETER", Sensor.TYPE_ACCELEROMETER},
                new Object[] {"Accelerometer", Sensor.TYPE_ACCELEROMETER},
                new Object[] {"type_accelerometer", null},
                new Object[] {"nonexistent_sensor", null},
                new Object[] {"", null},
        };
    }

    @Test
    @Parameters(method = "getValueWithDefaultParams")
    public void getValueWithDefault(String sensorName, Integer defaultValue, Integer expectedSensorType) {
        assertThat(sensorTypeMap.getValue(sensorName, defaultValue), is(expectedSensorType));
    }
    private Object getValueWithDefaultParams() {
        return new Object[] {
                new Object[] {"accelerometer", null, Sensor.TYPE_ACCELEROMETER},
                new Object[] {"type_accelerometer", Sensor.TYPE_ACCELEROMETER, Sensor.TYPE_ACCELEROMETER},
                new Object[] {"nonexistent_sensor", Sensor.TYPE_ACCELEROMETER, Sensor.TYPE_ACCELEROMETER},
                new Object[] {"", Sensor.TYPE_ACCELEROMETER, Sensor.TYPE_ACCELEROMETER},
        };
    }

    @Test
    @Parameters(method = "containsNameParams")
    public void containsName(String sensorName, boolean expectedValue) {
        assertThat(sensorTypeMap.containsName(sensorName), is(expectedValue));
    }
    private Object containsNameParams() {
        return new Object[] {
                new Object[] {"accelerometer", true},
                new Object[] {"ACCELEROMETER", true},
                new Object[] {"Accelerometer", true},
                new Object[] {"type_accelerometer", false},
                new Object[] {"nonexistent_sensor", false},
                new Object[] {"", false},
        };
    }

    @Test
    @Parameters(method = "getNameParams")
    public void getName(Integer sensorType, String expectedSensorName) {
        assertThat(sensorTypeMap.getName(sensorType), is(expectedSensorName));
    }
    private Object getNameParams() {
        return new Object[] {
                new Object[] {Sensor.TYPE_ACCELEROMETER, "accelerometer"},
                new Object[] {null, null},
                new Object[] {-123456, null},
        };
    }

    @Test
    @Parameters(method = "getNameWithDefaultParams")
    public void getNameWithDefault(Integer sensorType, String defaultName, String expectedSensorName) {
        assertThat(sensorTypeMap.getName(sensorType, defaultName), is(expectedSensorName));
    }
    private Object getNameWithDefaultParams() {
        return new Object[] {
                new Object[] {Sensor.TYPE_ACCELEROMETER, null, "accelerometer"},
                new Object[] {null, "accelerometer", "accelerometer"},
                new Object[] {-123456, "accelerometer", "accelerometer"},
        };
    }

    @Test
    @Parameters(method = "containsValueParams")
    public void containsType(Integer sensorType, boolean expectedValue) {
        assertThat(sensorTypeMap.containsValue(sensorType), is(expectedValue));
    }
    private Object containsValueParams() {
        return new Object[] {
                new Object[] {Sensor.TYPE_ACCELEROMETER, true},
                new Object[] {null, false},
                new Object[] {-123456, false},
        };
    }
}