package com.behametrics.logger.Utils;

import com.behametrics.logger.Utils.Properties.PropertyReader;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

@RunWith(JUnitParamsRunner.class)
public class PropertyReaderTest {

    private PropertyReader propertyReader;

    @Before
    public void setUp() {
        propertyReader = new PropertyReader();
    }

    @Test
    public void getStringPropertyForDefaultProperty() {
        assertThat(propertyReader.getStringProperty("Inputs").isEmpty(), is(false));
    }

    @Test
    @Parameters(method = "getStringPropertyParams")
    public void getStringProperty(String inputValue, String expectedValue) {
        propertyReader.setProperty("testProperty", inputValue);
        assertThat(propertyReader.getStringProperty("testProperty"), is(expectedValue));
    }
    private Object getStringPropertyParams() {
        return new Object[] {
                new Object[] {"value", "value"},
                new Object[] {"", ""},
        };
    }

    @Test
    public void getStringProperty_nonExistentProperty() {
        assertThat(propertyReader.getStringProperty("nonExistentProperty"), is(""));
    }

    @Test
    @Parameters(method = "getIntPropertyParams")
    public void getIntProperty(String inputValue, int expectedValue) {
        propertyReader.setProperty("testProperty", inputValue);
        assertThat(propertyReader.getIntProperty("testProperty"), is(expectedValue));
    }
    private Object getIntPropertyParams() {
        return new Object[] {
                new Object[] {"1", 1},
                new Object[] {"10", 10},
                new Object[] {"-1", -1},
                new Object[] {"", 0},
                new Object[] {"invalidInteger", 0},
        };
    }

    @Test
    public void getIntProperty_nonExistentProperty() {
        assertThat(propertyReader.getIntProperty("nonExistentProperty"), is(0));
    }

    @Test
    @Parameters(method = "getBooleanPropertyParams")
    public void getBooleanProperty(String inputValue, boolean expectedValue) {
        propertyReader.setProperty("testProperty", inputValue);
        assertThat(propertyReader.getBooleanProperty("testProperty"), is(expectedValue));
    }
    private Object getBooleanPropertyParams() {
        return new Object[] {
                new Object[] {"true", true},
                new Object[] {"True", true},
                new Object[] {"TRUE", true},
                new Object[] {"false", false},
                new Object[] {"False", false},
                new Object[] {"FALSE", false},
                new Object[] {"", false},
                new Object[] {"randomText", false},
        };
    }

    @Test
    public void getBooleanProperty_nonExistentProperty() {
        assertThat(propertyReader.getBooleanProperty("nonExistentProperty"), is(false));
    }

    @Test
    @Parameters(method = "getStringArrayPropertyParams")
    public void getStringArrayProperty(String inputValue, String separator, String[] expectedValue) {
        propertyReader.setProperty("testProperty", inputValue);
        assertThat(propertyReader.getStringArrayProperty("testProperty", separator), is(expectedValue));
    }
    private Object getStringArrayPropertyParams() {
        return new Object[] {
                new Object[] {"one", ",", new String[] {"one"}},
                new Object[] {"one,two", ",", new String[] {"one", "two"}},
                new Object[] {"one,two,three", ",", new String[] {"one", "two", "three"}},
                new Object[] {"one,,two", ",", new String[] {"one", "", "two"}},
                new Object[] {"one,two,", ",", new String[] {"one", "two"}},
                new Object[] {",one,two", ",", new String[] {"", "one", "two"}},
                new Object[] {"one, two", ",", new String[] {"one", " two"}},
                new Object[] {"one, two", ", ", new String[] {"one", "two"}},
                new Object[] {"one.two", ".", new String[] {"one", "two"}},
                new Object[] {"", ",", new String[] {""}},
                new Object[] {"one,two", "", new String[] {"one,two"}},
                new Object[] {"one,two", null, new String[] {"one,two"}},
        };
    }

    @Test
    @Parameters(method = "getStringArrayPropertyWithTrimmingWhitespaceParams")
    public void getStringArrayPropertyWithTrimmingWhitespace(String inputValue, String separator, String[] expectedValue) {
        propertyReader.setProperty("testProperty", inputValue);
        assertThat(
                propertyReader.getStringArrayProperty("testProperty", separator, true),
                is(expectedValue));
    }
    private Object getStringArrayPropertyWithTrimmingWhitespaceParams() {
        return new Object[] {
                new Object[] {"one", ",", new String[] {"one"}},
                new Object[] {" one", ",", new String[] {"one"}},
                new Object[] {"one ", ",", new String[] {"one"}},
                new Object[] {"  one ", ",", new String[] {"one"}},
                new Object[] {"one,two,three", ",", new String[] {"one", "two", "three"}},
                new Object[] {"one, ,two", ",", new String[] {"one", "", "two"}},
                new Object[] {"one,two, ", ",", new String[] {"one", "two", ""}},
                new Object[] {" ,one,two", ",", new String[] {"", "one", "two"}},
                new Object[] {"one, two", ",", new String[] {"one", "two"}},
                new Object[] {"one, two", ", ", new String[] {"one", "two"}},
                new Object[] {"one,  two", ", ", new String[] {"one", "two"}},
                new Object[] {" ", ",", new String[] {""}},
        };
    }

    @Test
    public void getStringArrayProperty_nonExistentProperty() {
        assertThat(
                propertyReader.getStringArrayProperty("nonExistentProperty", ",").length,
                is(0));
    }

    @Test
    public void copy() {
        propertyReader.setProperty("property1", "1");
        propertyReader.setProperty("property2", "2");
        propertyReader.setProperty("property3", "3");

        PropertyReader newPropertyReader = propertyReader.copy();
        propertyReader.setProperty("property1", "4");
        propertyReader.setProperty("property2", "5");
        propertyReader.setProperty("property3", "6");

        assertThat(newPropertyReader.getStringProperty("property1"), is("1"));
        assertThat(newPropertyReader.getStringProperty("property2"), is("2"));
        assertThat(newPropertyReader.getStringProperty("property3"), is("3"));
    }

    @Test
    public void copy_unmodifiedPropertyReader() {
        PropertyReader newPropertyReader = propertyReader.copy();
        assertThat(newPropertyReader.getProperties().isEmpty(), is(false));
    }
}