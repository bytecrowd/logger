package com.behametrics.logger.Utils;

import android.content.Context;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.File;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(JUnitParamsRunner.class)
public class StoragePathManagerTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private Context mockedContext;

    @Test
    @Parameters(method = "getStorageDirpath_internalParams")
    public void getStorageDirpath_internal(String storageType) {
        when(mockedContext.getFilesDir()).thenReturn(new File(""));

        String storageDirpath = StoragePathManager.getStorageDirpath(mockedContext, storageType);

        Mockito.verify(mockedContext, Mockito.times(1)).getFilesDir();
        Mockito.verifyNoMoreInteractions(mockedContext);

        assertThat(storageDirpath, is(notNullValue()));
    }
    private Object getStorageDirpath_internalParams() {
        return new Object[] {
                new Object[] {"internal"},
        };
    }

    @Test
    @Parameters(method = "getStorageDirpath_invalidStorageTypeParams")
    public void getStorageDirpath_invalidStorageType(String storageType) {
        when(mockedContext.getFilesDir()).thenReturn(new File(""));

        String storageDirpath = StoragePathManager.getStorageDirpath(mockedContext, storageType);

        Mockito.verify(mockedContext, Mockito.times(1)).getFilesDir();
        Mockito.verifyNoMoreInteractions(mockedContext);

        assertThat(storageDirpath, is(notNullValue()));
    }
    private Object getStorageDirpath_invalidStorageTypeParams() {
        return new Object[] {
                new Object[] {null},
                new Object[] {"invalid_type"},
        };
    }

    @Test
    @Parameters(method = "getStorageDirpath_primaryExternalParams")
    public void getStorageDirpath_primaryExternal(String storageType) {
        when(mockedContext.getExternalFilesDir(null)).thenReturn(new File(""));

        String storageDirpath = StoragePathManager.getStorageDirpath(mockedContext, storageType);

        Mockito.verify(mockedContext, Mockito.times(1)).getExternalFilesDir(null);
        Mockito.verifyNoMoreInteractions(mockedContext);

        assertThat(storageDirpath, is(notNullValue()));
    }
    private Object getStorageDirpath_primaryExternalParams() {
        return new Object[] {
                new Object[] {"external"},
        };
    }

    @Test
    @Parameters(method = "getStorageDirpath_externalStorageDoesNotExistParams")
    public void getStorageDirpath_externalStorageDoesNotExist(String storageType) {
        when(mockedContext.getExternalFilesDir(null)).thenReturn(null);
        when(mockedContext.getFilesDir()).thenReturn(new File(""));

        String storageDirpath = StoragePathManager.getStorageDirpath(mockedContext, storageType);

        Mockito.verify(mockedContext, Mockito.times(1)).getExternalFilesDir(null);
        Mockito.verify(mockedContext, Mockito.times(1)).getFilesDir();
        Mockito.verifyNoMoreInteractions(mockedContext);

        assertThat(storageDirpath, is(notNullValue()));
    }
    private Object getStorageDirpath_externalStorageDoesNotExistParams() {
        return new Object[] {
                new Object[] {"external"},
        };
    }

    @Test
    @Parameters(method = "getStorageDirpath_multipleExternalParams")
    public void getStorageDirpath_multipleExternal(String storageType) {
        when(mockedContext.getExternalFilesDirs(null)).thenReturn(new File[] {
                new File(""),
                new File(""),
                new File(""),
                new File("")});

        String storageDirpath = StoragePathManager.getStorageDirpath(mockedContext, storageType);

        Mockito.verify(mockedContext, Mockito.times(1)).getExternalFilesDirs(null);
        Mockito.verifyNoMoreInteractions(mockedContext);

        assertThat(storageDirpath, is(notNullValue()));
    }
    private Object getStorageDirpath_multipleExternalParams() {
        return new Object[] {
                new Object[] {"external_0"},
                new Object[] {"external_1"},
                new Object[] {"external_3"},
        };
    }

    @Test
    @Parameters(method = "getStorageDirpath_multipleExternalNonPrimaryIsNull_returnsPrimaryExternalParams")
    public void getStorageDirpath_multipleExternalNonPrimaryIsNull_returnsPrimaryExternal(String storageType) {
        when(mockedContext.getExternalFilesDirs(null)).thenReturn(new File[] {
                new File(""),
                null,
                new File(""),
                null});
        when(mockedContext.getExternalFilesDir(null)).thenReturn(new File(""));

        String storageDirpath = StoragePathManager.getStorageDirpath(mockedContext, storageType);

        Mockito.verify(mockedContext, Mockito.times(1)).getExternalFilesDirs(null);
        Mockito.verify(mockedContext, Mockito.times(1)).getExternalFilesDir(null);
        Mockito.verifyNoMoreInteractions(mockedContext);

        assertThat(storageDirpath, is(notNullValue()));
    }
    private Object getStorageDirpath_multipleExternalNonPrimaryIsNull_returnsPrimaryExternalParams() {
        return new Object[] {
                new Object[] {"external_1"},
                new Object[] {"external_3"},
        };
    }

    @Test
    @Parameters(method = "getStorageDirpath_multipleExternalNonPrimaryIsOutOfBounds_returnsPrimaryExternalParams")
    public void getStorageDirpath_multipleExternalNonPrimaryIsOutOfBounds_returnsPrimaryExternal(String storageType) {
        when(mockedContext.getExternalFilesDirs(null)).thenReturn(new File[] {
                new File(""),
                new File(""),
                new File(""),
                new File("")});
        when(mockedContext.getExternalFilesDir(null)).thenReturn(new File(""));

        String storageDirpath = StoragePathManager.getStorageDirpath(mockedContext, storageType);

        Mockito.verify(mockedContext, Mockito.times(1)).getExternalFilesDirs(null);
        Mockito.verify(mockedContext, Mockito.times(1)).getExternalFilesDir(null);
        Mockito.verifyNoMoreInteractions(mockedContext);

        assertThat(storageDirpath, is(notNullValue()));
    }
    private Object getStorageDirpath_multipleExternalNonPrimaryIsOutOfBounds_returnsPrimaryExternalParams() {
        return new Object[] {
                new Object[] {"external_5"},
                new Object[] {"external_-1"},
        };
    }

    @Test
    @Parameters(method = "getStorageDirpath_multipleExternalInvalidNumberParams")
    public void getStorageDirpath_multipleExternalInvalidNumber(String storageType) {
        when(mockedContext.getFilesDir()).thenReturn(new File(""));

        String storageDirpath = StoragePathManager.getStorageDirpath(mockedContext, storageType);

        Mockito.verify(mockedContext, Mockito.times(1)).getFilesDir();
        Mockito.verifyNoMoreInteractions(mockedContext);

        assertThat(storageDirpath, is(notNullValue()));
    }
    private Object getStorageDirpath_multipleExternalInvalidNumberParams() {
        return new Object[] {
                new Object[] {"external_"},
                new Object[] {"external_invalid"},
        };
    }
}