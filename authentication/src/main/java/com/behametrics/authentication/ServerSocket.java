package com.behametrics.authentication;

import android.content.Context;
import android.util.Log;
import android.util.Property;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import xdroid.toaster.Toaster;

import static java.lang.Boolean.TRUE;

/**
 * ServerSocket
 */
public class ServerSocket {
    private List<Socket> sockets;
    private final String uuid;
    private boolean auth = TRUE;
    private final String TAG = "ServerSocket";

    /**
     * Constructor for creating socket and establish connection
     *
     * @param socketUrl socket address
     * @param uuid      identification of mobile app
     * @param serverUrl server URL to send logged events to
     */
    public ServerSocket(String socketUrl, String uuid, String serverUrl) {
        Log.d(TAG, "Creating socket");

        this.uuid = uuid;
        sockets = new ArrayList<>();

        try {
            sockets.add(IO.socket(socketUrl));
            sockets.add(IO.socket(serverUrl));
        } catch (URISyntaxException e) {
            Log.e("ServerSocket", "Failed to create sockets");
        }

        sockets.get(0).on("learn", onLearnMessage);
        sockets.get(0).on("authenticate", onAuthMessage);

        for (Socket socket : sockets) {
            socket.connect();
        }

        connect(uuid);
    }

    /**
     * Function for sending authentication event
     */
    public void sendAuthenticate() {
        sockets.get(0).emit("authenticate", uuid);
    }

    /**
     * Function for sending learn event
     */
    public void sendLearn() {
        sockets.get(0).emit("learn", uuid);
    }

    /**
     * Function for sending uuid to connect
     *
     * @param uuid
     */
    private void connect(String uuid) {
        for (Socket socket : sockets) {
            socket.emit("connectSocket", uuid);
        }
    }

    /**
     * Function for disconnecting
     */
    public void disconnect() {
        Log.d(TAG, "Disconnecting from socket");
        for (Socket socket : sockets) {
            socket.emit("disconnect", uuid);
            socket.disconnect();
        }
        sockets.get(0).off("authenticate", onLearnMessage);
    }


    private Emitter.Listener onLearnMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d(TAG, "Response: " + String.valueOf(args[0]));
            Toaster.toast(args[0].toString());
        }
    };

    /**
     * EventListener on Authentication response
     */
    public Emitter.Listener onAuthMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d(TAG, "Response: " + String.valueOf(args[0]));
            Toaster.toast(String.valueOf(args[0]));
        }
    };

    public void setAuth(Emitter.Listener listener) {
        sockets.get(0).on("authenticate", listener);
    }
}
