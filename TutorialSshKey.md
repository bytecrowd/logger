## Behametrics
#### How to use our project (library) 
Open Git Bash
Generate RSA key by pasting the text bellow (substitute your mail on GitLab)
```
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```
This creates a new ssh key, using the provided email as a label
```
Generating public/private rsa key pair.
```
When you're prompted to "Enter a file in which to save the key," press Enter. 
This accepts the default file location
```
Enter a file in which to save the key (/c/Users/you/.ssh/id_rsa):[Press enter]
```
At the prompt, type secure passphrase. For more information, see ["Working with SSH key passphrases"](https://help.github.com/articles/working-with-ssh-key-passphrases),
or just press enter twice to continue without passphrase
```
Enter passphrase (empty for no passphrase): [Type a passphrase]
Enter same passphrase again: [Type passphrase again]
```

#### Adding your SSH key to the ssh-agent
Open GitBash and paste the text below (Ensure the ssh-agent is running)
```
eval $(ssh-agent -s)
```
You should see something like this
```
Agent pid 5784
```
Add your SHH private key to the ssh-agent
```
ssh-add ~/.ssh/id_rsa
```

#### Adding your SSH public key to GitLab
Open GitBash and paste the text below
(It'll copy your public key into clipboard)
```
clip < ~/.ssh/id_rsa.pub
```
Now just navigate to GitLab site -> Settings -> SSH Keys and paste (CTRL+V) your key into Key area.
Choose Title for a new SSH key, could be anything.
