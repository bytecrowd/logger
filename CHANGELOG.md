# 2.0.1

* Fixed potential memory leak when fragments are paused/stopped/destroyed.

# 2.0.0

* Added more fields to touch events, including raw X and Y and more touch events (e.g. `TOUCH_CANCEL`).
* Touch logging is now properly supported for `DialogFragment` objects.
* Provided functions to enable logging for `Dialog` and `PopupWindow` objects manually (for which logging apparently cannot be enabled automatically).
* Each input (touch, individual sensors, custom events, etc.) is now logged to a separate CSV files along with headers (except custom and lifecycle events).
* Reworked uploading logged events. The events are now uploaded as JSON objects. Uploading CSV files is no longer supported.
* Allowed toggling logging of touch and custom events.
* Allowed configuring the sensor log rate.
* Added logging of changes in device orientation (enabled by default).
* Added logging of lifecycle events (disabled by default).
* Allowed logging to external storage.
* All fields in touch events are now lowercase for consistency with other fields.
* Allowed toggling of displaying debug logs in the console (e.g. to avoid spamming the console during debug runs for apps using the logger). 
* Adjusted the demo application to test more features of the logger.

# 2.0.0-beta

* Major changes in logging. Make sure you check the [README](README.md) for updated usage.
    * Replaced sensor names with device-independent names. Removed redundant logging of sensor types as integers.
    * Added touch minor and touch major to touch data.
    * Logging is now always started upon starting or resuming an activity, and is stopped upon pausing or stopping an activity.
      This means that sensor logging in the background is no longer possible (for the time being, at least).
* Allowed configuring sensors to be logged.
  For the time being, logging touch data cannot be disabled and consequently will always be logged.
* By default, only touch, accelerometer, linear acceleration and gyroscope will be logged, to save performance.
  To log all sensors again, specify `"all"` in the `Sensors` entry in `config.properties.
* Adjusted default configuration settings.
* Reworked package hierarchy. Updated package paths for classes in applications.
* Fixed duplicate logging of events when switching between activities.
* Fixed potential memory leaks (associated with holding references to `Activity` instances).
* Major internal cleanup.
