package com.behametrics.processor;

import java.util.HashMap;


interface IProperty<N, V> {
    V put(N name, V value);

    V get(N name);

    boolean has(N name);

    V remove(N name);
}

interface IPropertyField<T> {
    Class<T> getType();
}


/*
 * Create properties class with ImmutableConfig
 * */
public class Properties<V> implements IProperty<String, V> {

    HashMap<String, V> attributes;

    @Override
    public V put(String name, V value) {
        return null;
    }

    @Override
    public V get(String name) {
        return null;
    }

    @Override
    public boolean has(String name) {
        return false;
    }

    @Override
    public V remove(String name) {
        return null;
    }

}
