package com.behametrics.processor;

import com.behametrics.annotations.Logger;
import com.behametrics.processor.Utils.Android;
import com.google.auto.service.AutoService;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.tools.Diagnostic;

/*
    Logger annotations
    Main processor included
* */
@AutoService(Processor.class)
public class LoggerProcessor extends AbstractProcessor {

    private static final String activityPackage[] = new String[]{"android.app.Activity"};

    private Elements elements;
    private Messager messager;
    private Filer filer;

    @Override
    public synchronized void init(ProcessingEnvironment env) {
        super.init(env);
        filer = env.getFiler();
        messager = env.getMessager();
        elements = env.getElementUtils();
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {

        Collection<? extends Element> classLogger = roundEnvironment.getElementsAnnotatedWith(Logger.class);

        for (Element element : classLogger) {
            Logger annotation = element.getAnnotation(Logger.class);

            //Properties generatedProperties = initializeProperties(annotation);

            String className = element.getSimpleName().toString() + "Logger";

            ClassName activity = Android.getClass("Activity");
            ClassName lifeTracker = Android.getClass("ActivityLifeTracker");

            MethodSpec init = MethodSpec.methodBuilder("init")
                    .addModifiers(Modifier.PUBLIC)
                    .addModifiers(Modifier.STATIC)
                    .addParameter(activity, "activity")
                    .addStatement("activity.getApplication().registerActivityLifecycleCallbacks(new $T())", lifeTracker)
                    .build();

            TypeSpec genClass = TypeSpec.classBuilder(className)
                    .addModifiers(Modifier.PUBLIC)
                    .addModifiers(Modifier.FINAL)
                    .addMethod(init)
                    .build();

            JavaFile javaFile = JavaFile.builder("com.behametrics.generated", genClass)
                    .build();

            messager.printMessage(Diagnostic.Kind.NOTE, genClass.toString());

            try {
                javaFile.writeTo(filer);
            } catch (IOException e) {
                messager.printMessage(Diagnostic.Kind.WARNING, e.getMessage());
            }

        }

        return true;
    }

    /*
    NOTE: Property logic of logger is inconsistent, scheduled
    private MethodSpec createPropertyBind()
    {
        return null;
    }
    */
    /*private Properties initializeProperties(Logger annotation)
    {
        Properties props = new Properties();

        return props;
    }*/

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }


    /*
     * Supported annotation types
     * */
    @Override
    public Set<String> getSupportedAnnotationTypes() {
        Set<String> annotations = new HashSet<>();
        annotations.add(Logger.class.getCanonicalName());

        return annotations;
    }
}
