package com.behametrics.processor.ProcessHandler;

import javax.lang.model.element.Element;

public abstract class IProcessHandler {
    String regIdentifier;

    // Call Super
    public IProcessHandler(String regIdentifier) {
        // Make sure that regIdentifier is unique
        this.regIdentifier = regIdentifier;

        // Register yourself for work
        register();
    }

    // Override this in subclasses
    public abstract void process(Element element);

    private void register() {

    }
}
