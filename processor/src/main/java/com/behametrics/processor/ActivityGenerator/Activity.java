package com.behametrics.processor.ActivityGenerator;

import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import javax.lang.model.element.Modifier;

import java.util.List;

/**
 * Activity builder wrapper
 */

public class Activity implements Cloneable {

    String className;
    TypeSpec virtualActivity;

    Activity() {

    }

    Activity(TypeSpec virtualActivity) {
        this.virtualActivity = virtualActivity;
    }

    Activity(String name,
             List<MethodSpec> methodSpecs,
             List<FieldSpec> fieldSpecs,
             List<String> imports) {
        className = name;
        TypeSpec.Builder builder = TypeSpec.classBuilder(name);

        fieldSpecs.forEach(field -> builder.addField(field));
        methodSpecs.forEach(method -> builder.addMethod(method));

        virtualActivity = builder.build();
    }

    @Override
    public int hashCode() {
        return className.hashCode();
    }

    @Override
    public Activity clone() {
        return new Activity(this.virtualActivity);
    }

    public static class Builder {
        String className;
        private Activity buildingActivity;
        private TypeSpec virtualClass;

        private List<MethodSpec> methodSpecs;
        private List<FieldSpec> fieldSpecs;
        private List<String> imports;

        private Builder(String className) {
            this.className = className;
        }

        public static Builder create(String className) {
            return new Builder(className);
        }

        public Activity build() {
            return new Activity(className, methodSpecs, fieldSpecs, imports);
        }

        public Builder addImport(String packageImport) {
            imports.add(packageImport);

            return this;
        }

        public Builder addMethod(MethodSpec method) {
            methodSpecs.add(method);

            return this;
        }

        public Builder addField(TypeName fieldType, String name, Modifier modifier) {
            fieldSpecs.add(FieldSpec.builder(fieldType, name, modifier).build());

            return this;
        }

    }

}
