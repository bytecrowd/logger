package com.behametrics.processor.Utils;

import com.squareup.javapoet.ClassName;

import java.util.HashMap;

public class Android {

    private static final HashMap<String, String> classTypes = new HashMap<String, String>() {{

        put("Activity", "android.app");

        put("ActivityLifeTracker", "com.behametrics.logger.Callbacks");

    }};

    public static ClassName getClass(String name) {
        String packageName = classTypes.get(name);

        return ClassName.get(packageName, name);
    }
}
