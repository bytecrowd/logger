package com.behametrics.processor.Utils;

import java.util.Enumeration;
import java.util.Properties;

public class PropertyEncoder {

    public static String encodeProperties(Properties properties) {
        String encodedSequence = new String();

        Enumeration p = properties.propertyNames();

        while (p.hasMoreElements()) {
            String key = (String) p.nextElement();
            String value = properties.getProperty(key);
            encodedSequence += addPair(key, value);
        }

        return encodedSequence;
    }

    private static String addPair(String key, String value) {
        return "\n" + key + "=" + value;
    }
}
